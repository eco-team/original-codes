#ifndef SPECIE_H
#define SPECIE_H

#include "Aleatory.hpp"
#include "Organism.hpp"
#include "diversity/genotypic/GenotypicDiversityMeasure.hpp"
#include "diversity/phenotypic/PhenotypicDiversityMeasure.hpp"

class Specie{
public:
	Specie()
	{
		m_pop = 0;
		m_size = 0;
	};
	Specie(int size, int dimensions, unsigned int evolutionaryPeriod, unsigned int maxEvaluations, PhenotypicDiversityMeasure * phenotypic, GenotypicDiversityMeasure * genotypic);
	~Specie();
	void init(ObjectiveFunction * objFunction, FitnessFunction * fitFunction);
	Organism & operator[](unsigned int index);
	Specie & operator = (const Specie &);
	int size();
	Organism getCurrentBest();
	Organism getGlobalBest();
	unsigned int getEvaluations();
	bool incrementEvaluations();
	void initEvolutionaryPeriod();
	bool hasEvolutionaryPeriod();
	void finishEvolutionaryPeriod();
	void findCurrentBest();
	double getGenotypicDiversity();
	double getFirstGenotypicDiversity();
	double getPhenotypicDiversity();
	Organism getCentroid();
	int getCurrentBestIndex();
	bool getRelacao();
private:
	PhenotypicDiversityMeasure * m_phenotypicFunction;
	GenotypicDiversityMeasure * m_genotypicFunction;
	Organism * m_pop;
	Organism m_globalBest;
	Organism m_centroid;
	Organism m_currentBest;
	double m_genotypic;
	double m_FirstGenotypic;
	double m_phenotypic;
	unsigned int m_evaluations;
	unsigned int m_evolutionaryPeriod;
	unsigned int m_period;
	unsigned int m_maxEvaluations;
	unsigned short int m_currentBestIndex;
	unsigned short int m_dimens;
	std::size_t m_size;
	bool m_relacao;
	void calcDiversities();
	void calcCentroid();
};

Specie::Specie(int size, int dimens, unsigned int evolutionaryPeriod, unsigned int maxEvaluations, PhenotypicDiversityMeasure * phenotypic, GenotypicDiversityMeasure * genotypic)
{
	m_size = size;
	m_dimens = dimens;
	m_pop = new Organism[m_size];
	m_globalBest = m_pop[0];
	m_currentBest = m_pop[0];
	m_evolutionaryPeriod = evolutionaryPeriod;
	m_maxEvaluations = maxEvaluations;
	m_phenotypicFunction = phenotypic;
	m_genotypicFunction = genotypic;
	m_genotypic = 0;
	m_FirstGenotypic = -1.0;
	m_phenotypic = 0;
	m_evaluations = 0;
}

Specie::~Specie()
{
	if(m_pop != 0)
	{
		delete [] m_pop;
		m_pop = 0;		
	}
}

void Specie::init(ObjectiveFunction * objFunction, FitnessFunction * fitFunction)
{
	unsigned short int d = 0;
	unsigned short int i = 0;
	Aleatory aleatory;
	m_currentBestIndex = m_size-1;
	for(i = m_size; i--; )
	{
		m_pop[i] = Organism(m_dimens);
		for(d = m_dimens; d-- ; )
		{
			m_pop[i][d] = aleatory.nextDouble(objFunction->getLowerBound(d),objFunction->getUpperBound(d));
		}
		m_pop[i].evaluate(objFunction,fitFunction);
		if(m_pop[i].getObjectiveFunction() < m_pop[m_currentBestIndex].getObjectiveFunction())
			m_currentBestIndex = i;
	}
	m_evaluations += m_size;
	m_currentBest = m_pop[m_currentBestIndex];
	m_globalBest = m_pop[m_currentBestIndex];
	
	//-----------------
	// Define que tipo de relação a população ira realizar
	//-----------------
	m_relacao = (aleatory.nextDouble(0,1) < 0.5);
}

Organism& Specie::operator[](unsigned int index) {
    return m_pop[index];
}

int Specie::size()
{
	return m_size;
}

Specie & Specie::operator = (const Specie & other)
{
	if (this == &other) {
		return *this;
	}
	if(m_size != other.m_size && m_pop != 0)
	{						
			delete [] m_pop;
			m_pop = 0;
	}
	m_size = other.m_size;
	m_dimens = other.m_dimens;
	m_evaluations = other.m_evaluations;
	m_evolutionaryPeriod = other.m_evolutionaryPeriod;
	m_period = other.m_period;
	m_maxEvaluations = other.m_maxEvaluations;
	m_currentBestIndex = other.m_currentBestIndex;
	m_currentBest = other.m_currentBest;
	m_globalBest = other.m_globalBest;
	m_phenotypicFunction = other.m_phenotypicFunction;
	m_genotypicFunction = other.m_genotypicFunction;
	m_phenotypic = other.m_phenotypic;
	m_genotypic = other.m_genotypic;
	m_relacao = other.m_relacao;
	m_FirstGenotypic = other.m_FirstGenotypic;
	if(m_pop == 0)
	{
			m_pop = new Organism[m_size];
	}
	for(int i = m_size; i--; )
	{
			m_pop[i] = other.m_pop[i];
	}

	return *this;
}


bool Specie::getRelacao()
{
		return m_relacao;
}

Organism Specie::getCurrentBest()
{
	Organism o;
	o = m_currentBest;
	return o;
}

Organism Specie::getGlobalBest()
{
	Organism o;
	o = m_globalBest;
	return o;
}

unsigned int Specie::getEvaluations()
{
	return m_evaluations;
}

bool Specie::incrementEvaluations()
{
	m_evaluations++;
	m_period++;
	return (m_period < m_evolutionaryPeriod) && (m_evaluations < m_maxEvaluations);
}

void Specie::initEvolutionaryPeriod()
{
	m_period = 0;
}

bool Specie::hasEvolutionaryPeriod()
{
	return (m_period < m_evolutionaryPeriod) && (m_evaluations < m_maxEvaluations);
}

void Specie::finishEvolutionaryPeriod()
{
	findCurrentBest();
	calcCentroid();
	calcDiversities();
}

void Specie::findCurrentBest()
{
	m_currentBestIndex = m_size - 1;
	for(unsigned short int i = m_size; i-- ; )
	{
		if(m_pop[i].getObjectiveFunction() < m_pop[m_currentBestIndex].getObjectiveFunction())
			m_currentBestIndex = i;
	}
	m_currentBest = m_pop[m_currentBestIndex];
	if(m_pop[m_currentBestIndex].getObjectiveFunction() < m_globalBest.getObjectiveFunction())		
		m_globalBest = m_pop[m_currentBestIndex];
}

double Specie::getGenotypicDiversity()
{
	return m_genotypic;
}

double Specie::getFirstGenotypicDiversity()
{
	return m_FirstGenotypic;
}

double Specie::getPhenotypicDiversity()
{
	return m_phenotypic;
}

void Specie::calcCentroid()
{
	unsigned short int j = 0;
	m_centroid = m_pop[0];
	double sum = 0;
	for (unsigned short int k = m_centroid.size(); k-- ; )//each variable
	{
		sum = 0.0;
		for (j = m_size; j-- ; ) //each solution
		{
			sum += m_pop[j][k];
		}
		m_centroid[k] =  sum / m_size;
	}
}

void Specie::calcDiversities()
{
	m_phenotypic = m_phenotypicFunction->calc(m_pop, m_size);
	m_genotypic = m_genotypicFunction->calc(m_pop, m_size);
	if(m_FirstGenotypic < 0.0)
	{
		m_FirstGenotypic = m_genotypic;
	}
}

Organism Specie::getCentroid()
{
	Organism o;
	o = m_centroid;
	return o;
}


int Specie::getCurrentBestIndex()
{
	return m_currentBestIndex;
}

#endif
