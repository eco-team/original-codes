#ifndef PHENOTYPIC_FACTORY_H
#define PHENOTYPIC_FACTORY_H

#include <stdexcept>
#include <string>

#include "PhenotypicDiversityMeasure.hpp"
#include "DefaultPhenotypicDiversityMeasure.hpp"

class PhenotypicFactory{
public:
	PhenotypicDiversityMeasure * get(std::string name);
};

PhenotypicDiversityMeasure * PhenotypicFactory::get(std::string name)
{
	if(name == "DEFAULT")
	{
		return new DefaultPhenotypicDiversityMeasure();
	}
	throw std::invalid_argument( "received invalid Fitness Function name" );
}

#endif
