#ifndef DEFAULT_PHENOTYPIC_DIVERSITY_MEASURE_H
#define DEFAULT_PHENOTYPIC_DIVERSITY_MEASURE_H

#include <string>

#include "PhenotypicDiversityMeasure.hpp"


class DefaultPhenotypicDiversityMeasure : public PhenotypicDiversityMeasure{
public:
	DefaultPhenotypicDiversityMeasure()
	{
		m_worstObj = 0;
		m_bestObj = 0;
		m_flag = false;
	};
	virtual double calc(Organism pop[], int size);
	virtual std::string getName()
	{
		return "Default Phenotypic Diversity Measure";
	};
private:
	double m_worstObj;
	double m_bestObj;
	bool m_flag;
	void orderby_min_fo(Organism pop[],int size, int vetor[]);
};

double DefaultPhenotypicDiversityMeasure::calc(Organism pop[], int size)
{
	double aux_1;
	double aux_2;
	unsigned int k = 0;
	if(!m_flag)
	{
		m_worstObj = pop[0].getObjectiveFunction();
		m_bestObj = pop[0].getObjectiveFunction();
		m_flag = true;
	}
	for(k = 0; k < size; k++)
	{
		if(pop[k].getObjectiveFunction() < m_bestObj)
		{
			m_bestObj = pop[k].getObjectiveFunction();
		}
		if(pop[k].getObjectiveFunction() > m_worstObj)
		{
			m_worstObj = pop[k].getObjectiveFunction();
		}
	}

	double vmd = m_worstObj - m_bestObj;
	if(vmd < 0.0)
	{
		vmd = vmd * -1.0;
	}
	vmd = (vmd / size);

	double virtual_fit[size];
	for(k = 0; k < size; k++)
	{
		virtual_fit[k] = m_bestObj + (vmd * (k));
	}			
	vmd = 0.0;

	for(k = 0; k < (size-1); k++)
	{
		aux_1 = (virtual_fit[k] -virtual_fit[k+1]);
		if(aux_1 < 0.0)
		{
			aux_1 = (aux_1 * -1.0);
		}
		vmd += log((double)1.0 + aux_1);
	}	
	

	int ordenacao[size];
	for(k = 0; k < size ; k++)
	{
		ordenacao[k] = k;
	}
		
	aux_2 = 0;
	
	orderby_min_fo(pop,size,ordenacao);

	for(k = 0; k < (size - 1) ; k++)
	{
		aux_1 = (pop[ordenacao[k]].getObjectiveFunction() -  pop[ordenacao[k+1]].getObjectiveFunction());
		if(aux_1 < 0.0)
		{
			aux_1 = aux_1 * -1.0;
		}
		aux_2 += log((double)1.0 + aux_1);
	}
	return ( aux_2 / vmd);
}

void DefaultPhenotypicDiversityMeasure::orderby_min_fo(Organism list[],int size, int vetor[]) {
    	unsigned short int comparacoes = 0;
    	unsigned short int trocas = 0;
	unsigned short int i = 0;
	unsigned short int j = 0;
	unsigned short int aux = 0;
	for(i=size-1; i >= 1; i--) {
		for(j=0; j < i ; j++) {
			comparacoes ++;
			if( list[vetor[j]].getObjectiveFunction() > list[vetor[j+1]].getObjectiveFunction() )
			{
				aux = vetor[j];
				vetor[j] = vetor[j+1];
				vetor[j+1] = aux;
				trocas++;
			}
		}
	}
}


#endif
