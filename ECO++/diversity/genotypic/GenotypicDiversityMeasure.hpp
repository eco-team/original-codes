#ifndef GENOTYPIC_DIVERSITY_MEASURE_H
#define GENOTYPIC_DIVERSITY_MEASURE_H

#include <string>

#include "../../Organism.hpp"


class GenotypicDiversityMeasure{
public:
	virtual double calc(Organism pop[], int size) = 0;
	virtual std::string getName() = 0;
};

#endif
