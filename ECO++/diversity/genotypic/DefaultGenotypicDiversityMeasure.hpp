#ifndef DEFAULT_GENOTYPIC_DIVERSITY_MEASURE_H
#define DEFAULT_GENOTYPIC_DIVERSITY_MEASURE_H

#include <string>

#include "../../Organism.hpp"
#include "GenotypicDiversityMeasure.hpp"


class DefaultGenotypicDiversityMeasure : public GenotypicDiversityMeasure{
public:
	DefaultGenotypicDiversityMeasure()
	{
		m_nmdf = 0;
	};
	virtual double calc(Organism pop[], int size);
	virtual std::string getName()
	{
		return "Default Genotypic Diversity Measure";
	};
private:
	double m_nmdf;
};

double DefaultGenotypicDiversityMeasure::calc(Organism pop[], int size)
{
	double diversity = 0;
	double aux_1 = 0;
	double aux_2 = 0;
	double buffer = 0;
	unsigned short int a = 0;
	unsigned short int b = 0;
	unsigned short int d = 0;
	for(a = 0; a < size; a++)
	{
		for(b = (a+1); b < size; b++)
		{
			aux_1 = 0;
			aux_1 = sqrt(aux_1);
			for(d = 0; d < pop[a].size(); d++)
			{				
				aux_1 += pow(pop[a][d] - pop[b][d], 2);
			}
			if(b == (a+1) || aux_2 > aux_1)
			{
				aux_2 = aux_1;
			}
		}
		diversity += log((double)1.0 + aux_2);	
	}	
	if(m_nmdf < diversity)
	{
		m_nmdf = diversity;
	}
	return diversity / m_nmdf;
}

#endif
