#ifndef OBJECTIVE_FUNCTION_FACTORY_H
#define OBJECTIVE_FUNCTION_FACTORY_H

#include <stdexcept>
#include <string>

#include "ObjectiveFunction.hpp"
#include "benchmarkFunctions/Rastrigin.hpp"
#include "benchmarkFunctions/SchafferF7.hpp"
#include "benchmarkFunctions/SchafferF6.hpp"
#include "benchmarkFunctions/Griewank.hpp"
#include "benchmarkFunctions/Ackley.hpp"
#include "benchmarkFunctions/Rosenbrock.hpp"
#include "benchmarkFunctions/Sphere.hpp"
#include "benchmarkFunctions/StretchedV.hpp"
#include "benchmarkFunctions/SchwefelsFunction2_22.hpp"
#include "benchmarkFunctions/Step.hpp"
#include "benchmarkFunctions/GeneralizedSchwefelsFunction2_26.hpp"
#include "benchmarkFunctions/GeneralizedPenalizedFunction1.hpp"
#include "benchmarkFunctions/GeneralizedPenalizedFunction2.hpp"
#include "benchmarkFunctions/LevyFunction.hpp"
#include "benchmarkFunctions/Zakharov.hpp"
#include "benchmarkFunctions/EggHolder.hpp"
#include "benchmarkFunctions/GeneralizedHolzman.hpp"
#include "benchmarkFunctions/Michalewitz.hpp"
#include "benchmarkFunctions/Multimod.hpp"
#include "benchmarkFunctions/Powell.hpp"
#include "benchmarkFunctions/Rana.hpp"
#include "benchmarkFunctions/Shubert.hpp"
#include "benchmarkFunctions/ShiftedSphere.hpp"
#include "benchmarkFunctions/ShiftedSchwefelProblem2_21.hpp"
#include "benchmarkFunctions/ShiftedRosenbrock.hpp"
#include "benchmarkFunctions/ShiftedRastrigin.hpp"
#include "benchmarkFunctions/ShiftedGriewank.hpp"
#include "benchmarkFunctions/ShiftedAckley.hpp"
#include "benchmarkFunctions/ShiftedSchaffer.hpp"
#include "benchmarkFunctions/MolecularPotentialEnergy.hpp"
#include "benchmarkFunctions/DixonPrice.hpp"
#include "benchmarkFunctions/RotatedHyperEllipsoid.hpp"
#include "benchmarkFunctions/Trid6.hpp"
#include "benchmarkFunctions/Trid10.hpp"
#include "benchmarkFunctions/SumofDifferentPowers.hpp"
#include "benchmarkFunctions/SumSquares.hpp"
#include "benchmarkFunctions/StyblinskiTang.hpp"
#include "benchmarkFunctions/Bukin7.hpp"
#include "benchmarkFunctions/CrossInTray.hpp"
#include "benchmarkFunctions/DropWave.hpp"
#include "benchmarkFunctions/HolderTable.hpp"
#include "benchmarkFunctions/Booth.hpp"
#include "benchmarkFunctions/Matyas.hpp"
#include "benchmarkFunctions/McCornick.hpp"
#include "benchmarkFunctions/ThreeHump.hpp"
#include "benchmarkFunctions/SixHump.hpp"
#include "benchmarkFunctions/Easom.hpp"
#include "benchmarkFunctions/Beale.hpp"
#include "benchmarkFunctions/Chichinadze.hpp"
#include "benchmarkFunctions/GoldsteinPrice.hpp"
#include "benchmarkFunctions/Hansen.hpp"
#include "benchmarkFunctions/Himmelblau.hpp"
#include "benchmarkFunctions/Leon.hpp"
#include "benchmarkFunctions/Price.hpp"
#include "benchmarkFunctions/Trecanni.hpp"
#include "benchmarkFunctions/Zettl.hpp"
#include "benchmarkFunctions/Gear.hpp"
#include "benchmarkFunctions/Colville.hpp"
#include "benchmarkFunctions/Plateau.hpp"
#include "benchmarkFunctions/PowerSum.hpp"
#include "benchmarkFunctions/Langermann.hpp"


class ObjectiveFunctionFactory{
public:
	ObjectiveFunction * get(std::string name);
};

ObjectiveFunction * ObjectiveFunctionFactory::get(std::string name)
{
	if(name == "RASTRIGIN")
	{
		return new Rastrigin();
	}
	else if(name == "SCHAFFER_F7")
	{
		return new SchafferF7();
	}
	else if(name == "SCHAFFER_F6")
	{
		return new SchafferF6();
	}
	else if(name == "GRIEWANK")
	{
		return new Griewank();
	}
	else if(name == "ACKLEY")
	{
		return new Ackley();
	}
	else if(name == "ROSENBROCK")
	{
		return new Rosenbrock();
	}
	else if(name == "SPHERE")
	{
		return new Sphere();
	}
	else if(name == "STRETCHEDV")
	{
		return new StretchedV();
	}
	else if(name == "SCHWEFELS_FUNCTION_2_22")
	{
		return new SchwefelsFunction2_22();
	}
	else if(name == "STEP")
	{
		return new Step();
	}
	else if(name == "GENERALIZED_SCHWEFELS_FUNCTION_2_26")
	{
		return new GeneralizedSchwefelsFunction2_26();
	}
	else if(name == "GENERALIZED_PENALIZED_FUNCTION_1")
	{
		return new GeneralizedPenalizedFunction1();
	}
	else if(name == "GENERALIZED_PENALIZED_FUNCTION_2")
	{
		return new GeneralizedPenalizedFunction2();
	}
	else if(name == "LEVY_FUNCTION")
	{
		return new LevyFunction();
	}
	else if(name == "ZAKHAROV")
	{
		return new Zakharov();
	}
	else if(name == "EGG_HOLDER")
	{
		return new EggHolder();
	}
	else if(name == "GENERALIZED_HOLZMAN")
	{
		return new GeneralizedHolzman();
	}
	else if(name == "MICHALEWITZ")
	{
		return new Michalewitz();
	}
	else if(name == "MULTIMOD")
	{
		return new Multimod();
	}
	else if(name == "POWELL")
	{
		return new Powell();
	}
	else if(name == "RANA")
	{
		return new Rana();
	}
	else if(name == "SHUBERT")
	{
		return new Shubert();
	}
	else if(name == "SHIFTED_SPHERE")
	{
		return new ShiftedSphere();
	}
	else if(name == "SHIFTED_SCHWEFEL_PROBLEM_2_21")
	{
		return new ShiftedSchwefelProblem2_21();
	}
	else if(name == "SHIFTED_ROSENBROCK")
	{
		return new ShiftedRosenbrock();
	}
	else if(name == "SHIFTED_RASTRIGIN")
	{
		return new ShiftedRastrigin();
	}
	else if(name == "SHIFTED_GRIEWANK")
	{
		return new ShiftedGriewank();
	}
	else if(name == "SHIFTED_ACKLEY")
	{
		return new ShiftedAckley();
	}
	else if(name == "SHIFTED_SCHAFFER")
	{
		return new ShiftedSchaffer();
	}
	else if(name == "MOLECULAR_POTENTIAL_ENERGY")
	{
		return new MolecularPotentialEnergy();
	}
	else if(name == "ROTATED_HYPER_ELLIPSOID")
	{
		return new RotatedHyperEllipsoid();
	}
	else if(name == "SUM_OF_DIFFERENT_POWERS")
	{
		return new SumofDifferentPowers();
	}
	else if(name == "SUM_SQUARES")
	{
		return new SumSquares();
	}
	else if(name == "TRID6")
	{
		return new Trid6();
	}
	else if(name == "TRID10")
	{
		return new Trid10();
	}
	else if(name == "DIXON_PRICE")
	{
		return new DixonPrice();
	}
	else if(name == "STYBLINSKI_TANG")
	{
		return new StyblinskiTang();
	}
	else if(name == "BUKIN_7")
	{
		return new Bukin7();
	}
	//else if(name == "CROSS_IN_TRAY")
	//{
	//	return new CrossInTray();
	//}
	else if(name == "DROP_WAVE")
	{
		return new DropWave();
	}	
	else if(name == "HOLDER_TABLE")
	{
		return new HolderTable();
	}
	else if(name == "BOOTH")
	{
		return new Booth();
	}
	else if(name == "MATYAS")
	{
		return new Matyas();
	}
	else if(name == "MCCORNICK")
	{
		return new McCornick();
	}
	else if(name == "THREE_HUMP")
	{
		return new ThreeHump();
	}
	else if(name == "SIX_HUMP")
	{
		return new SixHump();
	}
	else if(name == "EASOM")
	{
		return new Easom();
	}	
	else if(name == "BEALE")
	{
		return new Beale();
	}
	else if(name == "CHICHINADZE")
	{
		return new Chichinadze();
	}
	else if(name == "GOLDSTEIN_PRICE")
	{
		return new GoldsteinPrice();
	}
	else if(name == "HANSEN")
	{
		return new Hansen();
	}
	else if(name == "HIMMELBLAU")
	{
		return new Himmelblau();
	}
	else if(name == "LEON")
	{
		return new Leon();
	}
	else if(name == "PRICE")
	{
		return new Price();
	}
	else if(name == "TRECANNI")
	{
		return new Trecanni();
	}
	else if(name == "ZETTL")
	{
		return new Zettl();
	}
	else if(name == "GEAR")
	{
		return new Gear();
	}
	else if(name == "COLVILLE")
	{
		return new Colville();
	}
	else if(name == "PLATEAU")
	{
		return new Plateau();
	}
	else if(name == "POWERSUM")
	{
		return new PowerSum();
	}
	else if(name == "LANGERMANN")
	{
		return new Langermann();
	}
	throw std::invalid_argument( "received invalid Objective Function name" );
}

#endif
