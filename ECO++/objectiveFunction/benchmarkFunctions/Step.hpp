#ifndef STEP_H
#define STEP_H

#include<math.h>
#include<string>


class Step : public ObjectiveFunction{
public:
	Step()
	{
		m_lowerBound = -100;
		m_upperBound =  100;
	};
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Step::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	for (i = 0; i < size; i++)
        {
        	aux = (individual[i]+0.5);
		aux1 += aux*aux;
        }
    if(aux1 - getOptimalPoint(size) <= 10e-20)
	{
		aux1 = getOptimalPoint(size);
	}
	return aux1;
}

std::string Step::getName()
{
	return "Step";
}


double Step::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Step::getLowerBound(int index)
{
	return m_lowerBound;
}

double Step::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
