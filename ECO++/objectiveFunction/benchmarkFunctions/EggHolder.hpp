#ifndef EGG_HOLDER_H
#define EGG_HOLDER_H

#include<math.h>
#include<string>


class EggHolder : public ObjectiveFunction{
public:
	EggHolder()
	{
		m_lowerBound = -512;
		m_upperBound =  512;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -915.61991 * size + 862.10466;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double EggHolder::evaluate(double individual[], int size)
{
	double aux = 0;
	unsigned short int i;
	for (i = 0; i < size-1; i++)
        {
        	aux += -(individual[i+1] + 47.0) * sin(sqrt(fabs(individual[i+1] + individual[i] * 0.5 + 47.0))) + sin(sqrt(fabs(individual[i] - (individual[i+1] + 47.0)))) * (-individual[i]);
        }
    if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string EggHolder::getName()
{
	return "EggHolder";
}


double EggHolder::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double EggHolder::getLowerBound(int index)
{
	return m_lowerBound;
}

double EggHolder::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
