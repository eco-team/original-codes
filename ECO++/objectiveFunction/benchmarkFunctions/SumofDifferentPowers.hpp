#ifndef SUMOFDIFFERENTPOWERS_H
#define SUMOFDIFFERENTPOWERS_H

#include<math.h>
#include<string>


class SumofDifferentPowers : public ObjectiveFunction{
public:
	SumofDifferentPowers()
	{
		m_lowerBound = -1;
		m_upperBound =  1;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double SumofDifferentPowers::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	for(i = 0; i < size; i++)
	{
		aux += pow(abs(individual[i]),i+1);
	}
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string SumofDifferentPowers::getName()
{
	return "SumofDifferentPowers";
}


double SumofDifferentPowers::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double SumofDifferentPowers::getLowerBound(int index)
{
	return m_lowerBound;
}

double SumofDifferentPowers::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
