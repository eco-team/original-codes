#ifndef COLVILLE_H
#define COLVILLE_H

#include<math.h>
#include<string>


class Colville : public ObjectiveFunction{
public:
	Colville()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Colville::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	
	aux = 100*pow((pow(individual[1],2)-individual[2]),2) + pow((individual[1]-1),2) + pow((individual[1]-1),2) + 90*pow((pow(individual[3],2)-individual[4]),2) + 10.1 *(pow((individual[2] - 1),2) + pow((individual[4] -1),2)) + 19.8*(individual[2]-1)*(individual[4]-1);

	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Colville::getName()
{
	return "Colville";
}


double Colville::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Colville::getLowerBound(int index)
{
	return m_lowerBound;
}

double Colville::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
