#ifndef CHICHINADZE_H
#define CHICHINADZE_H

#include<math.h>
#include<string>


class Chichinadze : public ObjectiveFunction{
public:
	Chichinadze()
	{
		m_lowerBound = -30;
		m_upperBound =  30;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 43.31519;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Chichinadze::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	double pi = 3.14159265358979323846264338327950288419;
	unsigned short int i;
	
	aux = pow(individual[0],2) - 12*individual[0] + 11 + 10*cos(pi/2 * individual[0]) + 8*sin(5*pi*individual[0])-1/sqrt(5)*exp(-1*(pow((individual[1]-0.5),2)/2));

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Chichinadze::getName()
{
	return "Chichinadze";
}


double Chichinadze::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Chichinadze::getLowerBound(int index)
{
	return m_lowerBound;
}

double Chichinadze::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
