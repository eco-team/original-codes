#ifndef TRID10_H
#define TRID10_H

#include<math.h>
#include<string>


class Trid10 : public ObjectiveFunction{
public:
	Trid10()
	{
		m_lowerBound = -100;
		m_upperBound =  100;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -200;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Trid10::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	for(i=0; i<size/size + 9; i++)
	{
		aux += pow(individual[i]-1,2);
	}
	for(i=1; i<size/size + 9; i++)
	{
		aux1 += individual[i]*individual[i-1];
	}
	
	aux = aux - aux1;
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Trid10::getName()
{
	return "Trid10";
}


double Trid10::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Trid10::getLowerBound(int index)
{
	return m_lowerBound;
}

double Trid10::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
