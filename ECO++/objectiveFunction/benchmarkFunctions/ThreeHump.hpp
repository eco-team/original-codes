#ifndef THREEHUMP_H
#define THREEHUMP_H

#include<math.h>
#include<string>


class ThreeHump : public ObjectiveFunction{
public:
	ThreeHump()
	{
		m_lowerBound = -5;
		m_upperBound =  5;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double ThreeHump::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	aux = 2*pow(individual[0],2) - 1.05*pow(individual[0],4) + pow(individual[0],6)/6 + individual[0]*individual[1]+pow(individual[1],2);

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string ThreeHump::getName()
{
	return "ThreeHump";
}


double ThreeHump::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double ThreeHump::getLowerBound(int index)
{
	return m_lowerBound;
}

double ThreeHump::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
