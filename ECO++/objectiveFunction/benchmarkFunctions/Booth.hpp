#ifndef BOOTH_H
#define BOOTH_H

#include<math.h>
#include<string>


class Booth : public ObjectiveFunction{
public:
	Booth()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Booth::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	double aux2 = 0;
	unsigned short int i;
	
	aux1 = pow(individual[0] + 2*individual[1] - 7, 2);
	aux2 = pow(2*individual[0] + individual[1] - 5 ,2);
	aux = aux1 + aux2;

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Booth::getName()
{
	return "Booth";
}


double Booth::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Booth::getLowerBound(int index)
{
	return m_lowerBound;
}

double Booth::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
