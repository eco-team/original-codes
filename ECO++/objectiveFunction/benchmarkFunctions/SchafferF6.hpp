#ifndef SCHAFFER_F6_H
#define SCHAFFER_F6_H

#include<math.h>
#include<string>


class SchafferF6 : public ObjectiveFunction{
public:
	SchafferF6()
	{
		m_lowerBound = -100.0;
		m_upperBound =  100.0;
	};
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double SchafferF6::evaluate(double individual[], int size)
{
	double obj = 0;
	double obj1 = 0;
	double obj2 = 0;
	for(unsigned short int j = 0 ; j < (size-1); j++)
        {
		obj1 = pow( sin( sqrt( pow(individual[j],(double)2) + pow(individual[j+1],(double)2) ) ), (double)2) - 0.5;
		obj2 = pow( 1.0 + 0.001* ( pow(individual[j],(double)2) + pow(individual[j+1],(double)2) ), (double)2);
		obj = obj + (0.5 + obj1/obj2);
        }
    if(obj - getOptimalPoint(size) <= 10e-20)
	{
		obj = getOptimalPoint(size);
	}
	return obj;
}

std::string SchafferF6::getName()
{
	return "SchafferF6";
}


double SchafferF6::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double SchafferF6::getLowerBound(int index)
{
	return m_lowerBound;
}

double SchafferF6::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
