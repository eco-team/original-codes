#ifndef CROSSINTRAY_H
#define CROSSINTRAY_H

#include<math.h>
#include<string>


class CrossInTray : public ObjectiveFunction{
public:
	CrossInTray()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double CrossInTray::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	double aux2 = 0;
	double pi = 3.14159265358;
	aux1 = sin(individual[0]*sin(individual[1]));
	aux2 = exp(abs(100-sqrt(pow(individual[0],2)+pow(individual[1],2))/pi));
	aux = -0.0001 * pow((abs(aux1 * aux2) +1),0.1);
	//aux = -0.0001 * pow(abs(sin(individual[0])*sin(individual[1])*exp(abs(100-(sqrt(pow(individual[0],2)+pow(individual[1],2))/pi)))+1),0.1);
	//printf("%lf %lf \n", aux1, aux2);
	//printf("%lf\n", isnan(aux));
	//isnan(aux);
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string CrossInTray::getName()
{
	return "CrossInTray";
}


double CrossInTray::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double CrossInTray::getLowerBound(int index)
{
	return m_lowerBound;
}

double CrossInTray::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
