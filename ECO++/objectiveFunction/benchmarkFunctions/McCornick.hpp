#ifndef MCCORNICK_H
#define MCCORNICK_H

#include<math.h>
#include<string>


class McCornick : public ObjectiveFunction{
public:
	McCornick()
	{
		m_lowerBound =  -3;
		m_upperBound =   4;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -1.9133;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double McCornick::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	aux = sin(individual[0] + individual[1]) + pow(individual[0] - individual[1],2) - 1.5*individual[0] + 2.5*individual[1] + 1;

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string McCornick::getName()
{
	return "McCornick";
}


double McCornick::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double McCornick::getLowerBound(int index)
{
	return m_lowerBound;
}

double McCornick::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
