#ifndef SHIFTED_ROSENBROCK_H
#define SHIFTED_ROSENBROCK_H

#include <math.h>
#include <string>
#include "ShiftedRosenbrockData.h"


class ShiftedRosenbrock : public ObjectiveFunction{
public:
	ShiftedRosenbrock()
	{
		m_lowerBound = -100;
		m_upperBound =  100;
		m_fBias = 390.0;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 390.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
	double m_fBias;
};

double ShiftedRosenbrock::evaluate(double individual[], int size)
{
	double zx[size];
	double Fx = 0;
	unsigned short int i;
	for(i=0;i<size;i++)
	{
		zx[i] = individual[i] - rosenbrock[i] + 1;   
	}

	for(i=0;i<size-1;i++)
	{    
	        Fx = Fx + 100*( pow((pow(zx[i],2)-zx[i+1]) , 2) ) + pow((zx[i]-1) , 2);
	}
	Fx =  Fx + m_fBias;
	if(Fx - getOptimalPoint(size) <= 10e-20)
	{
		Fx = getOptimalPoint(size);
	}
	return Fx;
}

std::string ShiftedRosenbrock::getName()
{
	return "ShiftedRosenbrock";
}


double ShiftedRosenbrock::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double ShiftedRosenbrock::getLowerBound(int index)
{
	return m_lowerBound;
}

double ShiftedRosenbrock::getUpperBound(int index)
{
	return m_upperBound;
}


#endif
