#ifndef TRECANNI_H
#define TRECANNI_H

#include<math.h>
#include<string>


class Trecanni : public ObjectiveFunction{
public:
	Trecanni()
	{
		m_lowerBound = -5;
		m_upperBound =  5;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Trecanni::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;

	aux = pow(individual[0],4) + 4*pow(individual[0],3) + 4*pow(individual[0],2)+pow(individual[1],2);

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Trecanni::getName()
{
	return "Trecanni";
}


double Trecanni::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Trecanni::getLowerBound(int index)
{
	return m_lowerBound;
}

double Trecanni::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
