#ifndef SHIFTED_SCHWEFEL_PROBLEM_2_21_H
#define SHIFTED_SCHWEFEL_PROBLEM_2_21_H

#define abss(a)     (a<0 ? (-a) : a)

#ifndef _max
#define	_max(x, y)	((x) > (y) ? (x) : (y))
#endif

#include <math.h>
#include <string>
#include "ShiftedSchwefelProblem2_21Data.h"


class ShiftedSchwefelProblem2_21 : public ObjectiveFunction{
public:
	ShiftedSchwefelProblem2_21()
	{
		m_lowerBound = -100;
		m_upperBound =  100;
		m_fBias = -450.0;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -450.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
	double m_fBias;
};

double ShiftedSchwefelProblem2_21::evaluate(double individual[], int size)
{
	double z = 0;
	double Fx = 0;
	unsigned short int i;
	Fx = abss(individual[0]);
	for(i=1;i<size;i++){
		z = individual[i] - schwefel[i];
		Fx = _max(Fx , abss(z)); // _max include from cluster.h
	}
	Fx = Fx + m_fBias;
	if(Fx - getOptimalPoint(size) <= 10e-20)
	{
		Fx = getOptimalPoint(size);
	}
	return Fx;
}

std::string ShiftedSchwefelProblem2_21::getName()
{
	return "ShiftedSchwefelProblem2_21";
}


double ShiftedSchwefelProblem2_21::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double ShiftedSchwefelProblem2_21::getLowerBound(int index)
{
	return m_lowerBound;
}

double ShiftedSchwefelProblem2_21::getUpperBound(int index)
{
	return m_upperBound;
}


#endif
