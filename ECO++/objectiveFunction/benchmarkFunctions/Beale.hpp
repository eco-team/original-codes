#ifndef BEALE_H
#define BEALE_H

#include<math.h>
#include<string>


class Beale : public ObjectiveFunction{
public:
	Beale()
	{
		m_lowerBound = -4.5;
		m_upperBound =  4.5;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Beale::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	aux = pow((1.5 - individual[0] + individual[0]*individual[1]),2) + pow((2.25 - individual[0]+individual[0]*pow(individual[1],2)),2) + pow((2.625- individual[0]+individual[0]*pow(individual[1],3)),2);

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Beale::getName()
{
	return "Beale";
}


double Beale::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Beale::getLowerBound(int index)
{
	return m_lowerBound;
}

double Beale::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
