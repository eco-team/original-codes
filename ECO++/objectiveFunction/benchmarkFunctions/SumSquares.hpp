#ifndef SUMSQUARES_H
#define SUMSQUARES_H

#include<math.h>
#include<string>


class SumSquares : public ObjectiveFunction{
public:
	SumSquares()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double SumSquares::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	for(i=0; i<size; i++)
	{
		aux += i*(pow(individual[i],2));
	}
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string SumSquares::getName()
{
	return "SumSquares";
}


double SumSquares::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double SumSquares::getLowerBound(int index)
{
	return m_lowerBound;
}

double SumSquares::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
