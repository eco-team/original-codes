#ifndef MATYAS_H
#define MATYAS_H

#include<math.h>
#include<string>


class Matyas : public ObjectiveFunction{
public:
	Matyas()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Matyas::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	aux1 = 0.26*(pow(individual[0],2) + pow(individual[1],2));
	aux = 0.48*individual[0]*individual[1];
	aux = aux1 - aux;

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Matyas::getName()
{
	return "Matyas";
}


double Matyas::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Matyas::getLowerBound(int index)
{
	return m_lowerBound;
}

double Matyas::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
