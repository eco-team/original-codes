#ifndef StyblinskiTang_H
#define StyblinskiTang_H

#include<math.h>
#include<string>


class StyblinskiTang : public ObjectiveFunction{
public:
	StyblinskiTang()
	{
		m_lowerBound = -5;
		m_upperBound =  5;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -78.332;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double StyblinskiTang::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	for(i = 0; i<size; i++)
	{
		aux += pow(individual[i],4) - 16*pow(individual[i],2) + 5*individual[i];
	}
	aux = 0.5 * aux;
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string StyblinskiTang::getName()
{
	return "StyblinskiTang";
}


double StyblinskiTang::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double StyblinskiTang::getLowerBound(int index)
{
	return m_lowerBound;
}

double StyblinskiTang::getUpperBound(int index)
{
	return m_upperBound;
}

#endif

