#ifndef DIXONPRICE_H
#define DIXONPRICE_H

#include<math.h>
#include<string>


class DixonPrice : public ObjectiveFunction{
public:
	DixonPrice()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double DixonPrice::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	unsigned short int j = 0;
	aux = pow(individual[j]-1, 2);
	for (i = 1; i<size; i++)
	{
		aux1 += i * (pow(2*pow(individual[i], 2) - individual[i-1], 2));
	}

	aux = aux + aux1;
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string DixonPrice::getName()
{
	return "DixonPrice";
}


double DixonPrice::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double DixonPrice::getLowerBound(int index)
{
	return m_lowerBound;
}

double DixonPrice::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
