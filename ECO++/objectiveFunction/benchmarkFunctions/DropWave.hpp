#ifndef DROPWAVE_H
#define DROPWAVE_H

#include<math.h>
#include<string>


class DropWave : public ObjectiveFunction{
public:
	DropWave()
	{
		m_lowerBound = -5.12;
		m_upperBound =  5.12;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -1.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double DropWave::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	double aux2 = 0;
	unsigned short int i;
	
	aux1 = 1 + cos(12*sqrt(pow(individual[0],2)+pow(individual[1],2)));
	aux2 = 0.5*(pow(individual[0],2)+pow(individual[1],2))+2;
	aux = -1 * (aux1/aux2);

	
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string DropWave::getName()
{
	return "DropWave";
}


double DropWave::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double DropWave::getLowerBound(int index)
{
	return m_lowerBound;
}

double DropWave::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
