#ifndef ROTATEDHYPERELLIPSOID_H
#define ROTATEDHYPERELLIPSOID_H

#include<math.h>
#include<string>


class RotatedHyperEllipsoid : public ObjectiveFunction{
public:
	RotatedHyperEllipsoid()
	{
		m_lowerBound = -65.536;
		m_upperBound =  65.536;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double RotatedHyperEllipsoid::evaluate(double individual[], int size)
{
	double aux = 0;
	//double aux1 = 0;
	unsigned short int i;
	unsigned short int j;
	for (i = 0; i < size; i++)
	{
		for (j = 0; j < i; j++)
		{
			aux += pow(individual[j], 2);
		}
	}
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
	
}

std::string RotatedHyperEllipsoid::getName()
{
	return "RotatedHyperEllipsoid";
}


double RotatedHyperEllipsoid::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double RotatedHyperEllipsoid::getLowerBound(int index)
{
	return m_lowerBound;
}

double RotatedHyperEllipsoid::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
