#ifndef PLATEAU_H
#define PLATEAU_H

#include<math.h>
#include<string>


class Plateau : public ObjectiveFunction{
public:
	Plateau()
	{
		m_lowerBound =     0;
		m_upperBound =  5.12;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Plateau::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	for(i=0; i<=4; i++)
	{
		aux += floor(individual[i]);
	}
	aux = aux + 30;
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Plateau::getName()
{
	return "Plateau";
}


double Plateau::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Plateau::getLowerBound(int index)
{
	return m_lowerBound;
}

double Plateau::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
