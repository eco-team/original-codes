#ifndef HOLDERTABLE_H
#define HOLDERTABLE_H

#include<math.h>
#include<string>


class HolderTable : public ObjectiveFunction{
public:
	HolderTable()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -19.2085;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double HolderTable::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	double pi = 3.14159265358979323846264;
	unsigned short int i;
	
	aux1 = abs(sin(individual[0])*cos(individual[1])*exp(abs(1-sqrt(pow(individual[0],2)+pow(individual[1],2))/pi)));
	aux = -1*aux1;

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string HolderTable::getName()
{
	return "HolderTable";
}


double HolderTable::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double HolderTable::getLowerBound(int index)
{
	return m_lowerBound;
}

double HolderTable::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
