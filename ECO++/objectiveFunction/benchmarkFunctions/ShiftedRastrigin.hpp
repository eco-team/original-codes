#ifndef SHIFTED_RASTRIGIN_H
#define SHIFTED_RASTRIGIN_H

#include <math.h>
#include <string>
#include "ShiftedRastriginData.h"


class ShiftedRastrigin : public ObjectiveFunction{
public:
	ShiftedRastrigin()
	{
		m_lowerBound = -5.12;
		m_upperBound =  5.12;
		m_fBias = -330.0;
		PI = acos(-1.0);
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -330.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
	double m_fBias;
	double PI;
};

double ShiftedRastrigin::evaluate(double individual[], int size)
{
	double z = 0;
	double Fx = 0;
	unsigned short int i;

	for(i=0;i<size-1;i++)
	{    
	        z = individual[i] - rastrigin[i];
	        Fx = Fx + ( pow(z,2) - 10*cos(2*PI*z) + 10);
	}
	Fx =  Fx + m_fBias;
	
	if(Fx - getOptimalPoint(size) <= 10e-20)
	{
		Fx = getOptimalPoint(size);
	}
	return Fx;
}

std::string ShiftedRastrigin::getName()
{
	return "ShiftedRastrigin";
}


double ShiftedRastrigin::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double ShiftedRastrigin::getLowerBound(int index)
{
	return m_lowerBound;
}

double ShiftedRastrigin::getUpperBound(int index)
{
	return m_upperBound;
}


#endif
