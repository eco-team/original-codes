#ifndef SIXHUMP_H
#define SIXHUMP_H

#include<math.h>
#include<string>


class SixHump : public ObjectiveFunction{
public:
	SixHump()
	{
		m_lowerBound = -3;
		m_upperBound =  3;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -1.0316;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double SixHump::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	aux = (4 - 2.1*pow(individual[0],2)+pow(individual[0],4)/3) * pow(individual[0],2)+individual[0]*individual[1]+(-4+4*pow(individual[1],2))*pow(individual[1],2);

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string SixHump::getName()
{
	return "SixHump";
}


double SixHump::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double SixHump::getLowerBound(int index)
{
	return m_lowerBound;
}

double SixHump::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
