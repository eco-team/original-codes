#ifndef HANSEN_H
#define HANSEN_H

#include<math.h>
#include<string>


class Hansen : public ObjectiveFunction{
public:
	Hansen()
	{
		m_lowerBound =   0;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -176.54;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Hansen::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	double aux2 = 0;
	unsigned short int i;
	unsigned short int j;
	for (i = 0; i <= 4; i++)
	{
		aux1 += (i+1)*cos(i*individual[0]+i+1);
	}
	for(j = 0; j<=4;j++)
	{
		aux2 += (j+1)*cos((j+2)*individual[1]+j+1);
	}
	
	aux = aux1 * aux2;
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Hansen::getName()
{
	return "Hansen";
}


double Hansen::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Hansen::getLowerBound(int index)
{
	return m_lowerBound;
}

double Hansen::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
