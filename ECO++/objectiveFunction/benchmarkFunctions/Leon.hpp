#ifndef LEON_H
#define LEON_H

#include<math.h>
#include<string>


class Leon : public ObjectiveFunction{
public:
	Leon()
	{
		m_lowerBound =   0;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Leon::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	aux = 100*pow((individual[1] - pow(individual[0],3)),2) + pow((individual[0] - 1.0),2);

	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Leon::getName()
{
	return "Leon";
}


double Leon::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Leon::getLowerBound(int index)
{
	return m_lowerBound;
}

double Leon::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
