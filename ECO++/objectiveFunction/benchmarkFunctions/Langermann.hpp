#ifndef LANGERMANN_H
#define LANGERMANN_H

#include<math.h>
#include<string>


class Langermann : public ObjectiveFunction{
public:
	Langermann()
	{
		m_lowerBound = 	 0;
		m_upperBound =   10;
		PI = 3.14159265;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -1.4;
	};
private:
	double m_lowerBound;
	double m_upperBound;
	double PI;
};

double Langermann::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	int c[5] = {1, 2, 5, 2, 3};
	int m = 5;
	int A[5][2] = {  
    {3, 5} ,  
    {5, 2} ,  
    {2, 1} ,
    {1, 4} ,
    {7, 9}
	};
	
	for(int i = 0; i < m; i++)
	{
		for (int j = 0; j < size; j++)
		{
			aux1 += pow((individual[j] - A[i][j]),2);
		}
		aux += c[i] * exp((-1.0/PI)*aux1) * cos(PI * aux1);
	}


	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Langermann::getName()
{
	return "Langermann";
}


double Langermann::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Langermann::getLowerBound(int index)
{
	return m_lowerBound;
}

double Langermann::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
