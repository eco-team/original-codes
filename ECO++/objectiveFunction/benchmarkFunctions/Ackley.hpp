#ifndef ACKLEY_H
#define ACKLEY_H

#include<math.h>
#include<string>


class Ackley : public ObjectiveFunction{
public:
	Ackley()
	{
		m_lowerBound = -32;
		m_upperBound =  32;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Ackley::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	for (i = 0; i < size; i++)
	{
		aux += individual[i]*individual[i];
	}
	for (i = 0; i < size; i++)
	{
		aux1 += cos(2.0*M_PI*individual[i]);
	}

	aux =  (-20.0*(exp(-0.2*sqrt(1.0/(float)size*aux)))-exp(1.0/(float)size*aux1)+20.0+exp(1));
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Ackley::getName()
{
	return "Ackley";
}


double Ackley::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Ackley::getLowerBound(int index)
{
	return m_lowerBound;
}

double Ackley::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
