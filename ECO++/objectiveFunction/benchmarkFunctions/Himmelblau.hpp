#ifndef HIMMELBLAU_H
#define HIMMELBLAU_H

#include<math.h>
#include<string>


class Himmelblau : public ObjectiveFunction{
public:
	Himmelblau()
	{
		m_lowerBound = -6;
		m_upperBound =  6;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Himmelblau::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	aux = pow((pow(individual[0],2) + individual[1] - 11),2) + pow((individual[0] + pow(individual[1],2) -7),2);

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Himmelblau::getName()
{
	return "Himmelblau";
}


double Himmelblau::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Himmelblau::getLowerBound(int index)
{
	return m_lowerBound;
}

double Himmelblau::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
