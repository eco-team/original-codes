#ifndef MICHALEWITZ_H
#define MICHALEWITZ_H

#include<math.h>
#include<string>


class Michalewitz : public ObjectiveFunction{
public:
	Michalewitz()
	{
		PI = 3.14159265;
		m_lowerBound =  0.0;
		m_upperBound =  PI;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -0.99864*size + 0.30271;
	};
private:
	double m_lowerBound;
	double m_upperBound;
	double PI;
};

double Michalewitz::evaluate(double individual[], int size)
{
	double aux = 0;
	unsigned short int i;
	for (i = 0; i < size; i++)
        {
        	aux = aux + sin(individual[i]) * pow(sin((i+1)*individual[i]*individual[i]/(float)PI), 2.0 * 10.0);
        }
	aux =  (-1*aux);
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Michalewitz::getName()
{
	return "Michalewitz";
}


double Michalewitz::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Michalewitz::getLowerBound(int index)
{
	return m_lowerBound;
}

double Michalewitz::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
