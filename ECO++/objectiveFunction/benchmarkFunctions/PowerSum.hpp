#ifndef POWERSUM_H
#define POWERSUM_H

#include<math.h>
#include<string>


class PowerSum : public ObjectiveFunction{
public:
	PowerSum()
	{
		m_lowerBound = 	 0;
		m_upperBound =   4;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double PowerSum::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	int b[4] = {8,18,44,114};
	for(int i = 0; i< 4; i++)
	{
		for (int j = 0; j < 4; ++j)
		{
			aux1 += pow(individual[j],i);
		}
		aux += pow((aux1 - b[i]),2);

	}


	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string PowerSum::getName()
{
	return "PowerSum";
}


double PowerSum::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double PowerSum::getLowerBound(int index)
{
	return m_lowerBound;
}

double PowerSum::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
