#ifndef SCHAFFER_F7_H
#define SCHAFFER_F7_H

#include<math.h>
#include<string>


class SchafferF7 : public ObjectiveFunction{
public:
	SchafferF7()
	{
		m_lowerBound = -100.0;
		m_upperBound =  100.0;
	};
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double SchafferF7::evaluate(double individual[], int size)
{
	double obj = 0;
	double obj1 = 0;
	for(unsigned short int j = 0 ; j < size; j++)
        {
            	obj = obj + pow(individual[j],(double)2);
            	obj1 = obj1 + pow(individual[j],(double)2);
        }
	obj = pow(obj,0.25);
	obj1 = pow(obj1,0.1);
	obj1 = pow(sin(50*obj1),(double)2) + 1.0;
	obj =  obj * obj1;
	
    if(obj - getOptimalPoint(size) <= 10e-20)
	{
		obj = getOptimalPoint(size);
	}
	return obj;
}

std::string SchafferF7::getName()
{
	return "SchafferF7";
}


double SchafferF7::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double SchafferF7::getLowerBound(int index)
{
	return m_lowerBound;
}

double SchafferF7::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
