#ifndef SHIFTED_SCHAFFER_H
#define SHIFTED_SCHAFFER_H

#include <math.h>
#include <string>
#include "ShiftedSchafferData.h"


class ShiftedSchaffer : public ObjectiveFunction{
public:
	ShiftedSchaffer()
	{
		m_lowerBound = -100;
		m_upperBound =  100;
		m_fBias = -450.0;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
	double m_fBias;
};

double ShiftedSchaffer::evaluate(double individual[], int size)
{
	unsigned short int i=0;
	double sum;
	double aux, aux2;
	double currentGen, nextGen;
	sum = 0.0;
	currentGen = individual[0]-data_shif_schaffer[i];
	currentGen = currentGen * currentGen;

	for (i = 1; i < size; i++)
	{
		nextGen = individual[i]-data_shif_schaffer[i];
		nextGen = nextGen * nextGen;
		aux = currentGen + nextGen;
		currentGen = nextGen;
		aux2 = sin(50. * pow(aux, 0.1));
		sum += pow(aux, 0.25) * (aux2 * aux2 + 1.0);
	}
	if(sum - getOptimalPoint(size) <= 10e-20)
	{
		sum = getOptimalPoint(size);
	}
	return sum;
}

std::string ShiftedSchaffer::getName()
{
	return "ShiftedSchaffer";
}


double ShiftedSchaffer::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double ShiftedSchaffer::getLowerBound(int index)
{
	return m_lowerBound;
}

double ShiftedSchaffer::getUpperBound(int index)
{
	return m_upperBound;
}


#endif
