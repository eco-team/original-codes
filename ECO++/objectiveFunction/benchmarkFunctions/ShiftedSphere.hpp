#ifndef SHIFTED_SPHERE_H
#define SHIFTED_SPHERE_H

#include <math.h>
#include <string>
#include "ShiftedSphereData.h"


class ShiftedSphere : public ObjectiveFunction{
public:
	ShiftedSphere()
	{
		m_lowerBound = -100;
		m_upperBound =  100;
		m_fBias = -450.0;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -450.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
	double m_fBias;
};

double ShiftedSphere::evaluate(double individual[], int size)
{
	double z = 0;
	double Fx = 0;
	unsigned short int i;
	for (i = 0; i < size; i++)
        {
        	z = individual[i] - sphere[i];
	        Fx += z*z;
        }
	Fx = Fx + m_fBias;	
	if(Fx - getOptimalPoint(size) <= 10e-20)
	{
		Fx = getOptimalPoint(size);
	}
	return Fx;
}

std::string ShiftedSphere::getName()
{
	return "ShiftedSphere";
}


double ShiftedSphere::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double ShiftedSphere::getLowerBound(int index)
{
	return m_lowerBound;
}

double ShiftedSphere::getUpperBound(int index)
{
	return m_upperBound;
}


#endif
