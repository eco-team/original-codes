#ifndef RASTRIGIN_H
#define RASTRIGIN_H

#include<math.h>
#include<string>


class Rastrigin : public ObjectiveFunction{
public:
	Rastrigin()
	{
		m_lowerBound = -5.12;
		m_upperBound =  5.12;
	};
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Rastrigin::evaluate(double individual[], int size)
{
	double obj = 0;
	for(unsigned short int j = 0 ; j < size; j++)
        {
            	obj = obj + (pow(individual[j],(double)2)-10*cos(2*M_PI*individual[j])+10);
        }
	if(obj - getOptimalPoint(size) <= 10e-20)
	{
		obj = getOptimalPoint(size);
	}
	return obj;
}

std::string Rastrigin::getName()
{
	return "Rastrigin";
}


double Rastrigin::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Rastrigin::getLowerBound(int index)
{
	return m_lowerBound;
}

double Rastrigin::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
