#ifndef EASOM_H
#define EASOM_H

#include<math.h>
#include<string>


class Easom : public ObjectiveFunction{
public:
	Easom()
	{
		m_lowerBound = -100;
		m_upperBound =  100;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -1.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Easom::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	double pi = 3.1415926535897932384626433832795028841971693;
	unsigned short int i;
	
	aux = -1*cos(individual[0])*cos(individual[1])*exp(-1*pow((individual[0]-pi),2)-pow((individual[1]-pi),2));

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Easom::getName()
{
	return "Easom";
}


double Easom::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Easom::getLowerBound(int index)
{
	return m_lowerBound;
}

double Easom::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
