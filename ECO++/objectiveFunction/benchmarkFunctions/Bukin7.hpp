#ifndef BUKIN7_H
#define BUKIN7_H

#include<math.h>
#include<string>


class Bukin7 : public ObjectiveFunction{
public:
	Bukin7()
	{
		m_lowerBound = -15;
		m_upperBound =   3;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Bukin7::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	aux = 100*sqrt(abs(individual[1] - 0.01*individual[0])) + 0.001*abs(individual[0] + 10);
	
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Bukin7::getName()
{
	return "Bukin7";
}


double Bukin7::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Bukin7::getLowerBound(int index)
{
	return m_lowerBound;
}

double Bukin7::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
