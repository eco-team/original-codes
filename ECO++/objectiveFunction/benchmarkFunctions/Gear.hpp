#ifndef GEAR_H
#define GEAR_H

#include<math.h>
#include<string>


class Gear : public ObjectiveFunction{
public:
	Gear()
	{
		m_lowerBound =  12;
		m_upperBound =  60;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 2.7e-12;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Gear::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	aux = 1.0/6.931 - floor(individual[0]) * floor(individual[1])/pow((floor(individual[2]) * floor(individual[3])),2);

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Gear::getName()
{
	return "Gear";
}


double Gear::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Gear::getLowerBound(int index)
{
	return m_lowerBound;
}

double Gear::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
