#ifndef SCHWEFELS_FUNCTION_2_22_H
#define SCHWEFELS_FUNCTION_2_22_H

#include<math.h>
#include<string>


class SchwefelsFunction2_22 : public ObjectiveFunction{
public:
	SchwefelsFunction2_22()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double SchwefelsFunction2_22::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	for (i = 0; i < size; i++)
        {
        	aux += fabs(individual[i]);
		aux1 *= fabs(individual[i]);
        }
	aux =  (aux+aux1);
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string SchwefelsFunction2_22::getName()
{
	return "SchwefelsFunction2_22";
}


double SchwefelsFunction2_22::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double SchwefelsFunction2_22::getLowerBound(int index)
{
	return m_lowerBound;
}

double SchwefelsFunction2_22::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
