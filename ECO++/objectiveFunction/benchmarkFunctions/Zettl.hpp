#ifndef ZETTL_H
#define ZETTL_H

#include<math.h>
#include<string>


class Zettl : public ObjectiveFunction{
public:
	Zettl()
	{
		m_lowerBound =   0;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -0.003791;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Zettl::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	aux = pow((pow(individual[0],2) + pow(individual[1],2) - 2*individual[0]),2) + 0.25*individual[0];

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Zettl::getName()
{
	return "Zettl";
}


double Zettl::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Zettl::getLowerBound(int index)
{
	return m_lowerBound;
}

double Zettl::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
