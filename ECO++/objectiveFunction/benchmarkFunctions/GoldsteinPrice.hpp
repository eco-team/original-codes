#ifndef GOLDSTEINPRICE_H
#define GOLDSTEINPRICE_H

#include<math.h>
#include<string>


class GoldsteinPrice : public ObjectiveFunction{
public:
	GoldsteinPrice()
	{
		m_lowerBound =  0;
		m_upperBound =  2;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 3.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double GoldsteinPrice::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	aux = (1 + pow((individual[0]+individual[1]+1),2)*(19 - 14*individual[0] + 3*pow(individual[0],2)-14*individual[1]+6*individual[0]*individual[1]+ 3*pow(individual[1],2)))*(30+pow(2*individual[0]-3*individual[1],2)*(18 - 32*individual[0] + pow(individual[0],2) +48*individual[1] - 36*individual[0]*individual[1] + 27*pow(individual[1],2)));

	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string GoldsteinPrice::getName()
{
	return "GoldsteinPrice";
}


double GoldsteinPrice::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double GoldsteinPrice::getLowerBound(int index)
{
	return m_lowerBound;
}

double GoldsteinPrice::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
