#ifndef ELD_H
#define ELD_H

#include<math.h>
#include<stdlib.h>
#include<string>
#include "../../Utils.hpp"

/* Structs */
typedef struct {
  double a;           /* Cost coefficient a */
  double b;           /* Cost coefficient b */
  double c;           /* Cost coefficient c */
  double e;           /* Cost coefficient e */
  double f;           /* Cost coefficient f */
  double Pmin;        /* Minimum fuel limit */
  double Pmax;        /* Maximum fuel limit */
} fuel_t;

typedef struct {
  double Pmin;        /* Minimum production limit */
  double Pmax;        /* Maximum production limit */
  double a;           /* Cost coefficient a */
  double b;           /* Cost coefficient b */
  double c;           /* Cost coefficient c */
  double e;           /* Cost coefficient e */
  double f;           /* Cost coefficient f */
  double P0;          /* Previous output power */
  double UR;          /* Upramp limit */
  double DR;          /* Downramp limit */
  unsigned int nPZ;   /* Number of prohibited zones */
  double *pzMin;      /* Lower bound of prohibited zone */
  double *pzMax;      /* Upper bound of prohibited zone */
  double *B;          /* Loss coefficients */
  double B0;          /* Loss coefficients */
  unsigned int nFuel; /* Number of fuel types */
  fuel_t *fuels;      /* Fuel array */
} g_t;


class Eld : public ObjectiveFunction{
public:
	Eld(std::string file, std::string prefix)
	{
		m_file = file;
		m_prefix = prefix;
		getParameters();
		//printParameters();
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
private:
	void getParameters();
	void printParameters();
	std::string m_file;
	std::string m_prefix;
	unsigned int m_numberOfGenerators;
	double m_totalDemandSystem;
	bool m_constraintPowerLosses;
	bool m_constraintProhibitedZones;
	bool m_constraintRampRateLimits;
	bool m_constraintValvePointEffects;
	bool m_constraintMultipleFuels;
	double m_penaltyBalance;
	double m_penaltyCapacities;
	double m_penaltyRampRateLimits;
	double m_penaltyProhibitedZones;
	double m_lossCoefficient;
	g_t * m_generators;
};

//==============================================
// Leitura do arquivo de parâmetros
//==============================================
void Eld::getParameters()
{
	int returnValue;
	int idFuel;
	int nGPZ;
	int idGPZ;
	unsigned int i,j;
	FILE * file;
	char aux;
	
	
	file = fopen(m_file.c_str(), "r");
	if(file == NULL) 
	{ 
		std::cout <<"Error opening ELD config file." << std::endl; 
		exit(1); 
	}
	
	returnValue = fscanf(file, "GENERAL PARAMETERS\n");
	returnValue = fscanf(file, "Number of generators: %i\n", &m_numberOfGenerators);
	m_generators = (g_t*) malloc(m_numberOfGenerators * sizeof(g_t));
	returnValue = fscanf(file, "Total demand: %lf\n", &m_totalDemandSystem);
	returnValue = fscanf(file, "CONSTRAINTS\n");
	returnValue = fscanf(file, "Power losses: %c\n", &aux);
	m_constraintPowerLosses = (aux == 'y') || (aux == 'Y');
	returnValue = fscanf(file, "Prohibited zones: %c\n", &aux);
	m_constraintProhibitedZones = (aux == 'y') || (aux == 'Y');
	returnValue = fscanf(file, "Ramp rate limits: %c\n", &aux);
	m_constraintRampRateLimits = (aux == 'y') || (aux == 'Y');
	returnValue = fscanf(file, "Valve point effects: %c\n", &aux);
	m_constraintValvePointEffects = (aux == 'y') || (aux == 'Y');
	returnValue = fscanf(file, "Multiple fuels: %c\n", &aux); 
	m_constraintMultipleFuels = (aux == 'y') || (aux == 'Y');
	returnValue = fscanf(file, "PENALTY PARAMETERS\n");
	returnValue = fscanf(file, "Penalty balance: %lf\n", &m_penaltyBalance);
	returnValue = fscanf(file, "Penalty capacities: %lf\n", &m_penaltyCapacities);
	if(m_constraintRampRateLimits)
	{
		returnValue = fscanf(file, "Penalty ramp rate limits: %lf\n", &m_penaltyRampRateLimits);
	}
	if(m_constraintProhibitedZones)
	{
		returnValue = fscanf(file, "Penalty prohibited zones: %lf\n", &m_penaltyProhibitedZones);
	}
	returnValue = fscanf(file, "OTHER PARAMETERS\n\n");
	returnValue = fscanf(file, "MINIMUM PRODUCTION LIMITS\n");
	for(i = 0; i < m_numberOfGenerators; ++i)
	{
		returnValue = fscanf(file, "%lf\n", &m_generators[i].Pmin);
	}
	returnValue = fscanf(file, "MAXIMUM PRODUCTION LIMITS\n");
	for(i = 0; i < m_numberOfGenerators; ++i)
	{
		returnValue = fscanf(file, "%lf\n", &m_generators[i].Pmax);
	}
	if(m_constraintMultipleFuels) {
		returnValue = fscanf(file, "FUEL ZONES\n");
		//std::cout << " FUEL ZONES "<< std::endl;
		for(i = 0; i < m_numberOfGenerators; ++i) {
			returnValue = fscanf(file, "%i ", &idFuel);
			idFuel--;
			returnValue = fscanf(file, "%i\n", &m_generators[idFuel].nFuel);
			m_generators[idFuel].fuels = (fuel_t*) malloc(m_generators[idFuel].nFuel * sizeof(fuel_t));
			for(j = 0; j < m_generators[idFuel].nFuel; ++j)
			{
				returnValue = fscanf(file, "%lf %lf\n", &m_generators[idFuel].fuels[j].Pmin, &m_generators[idFuel].fuels[j].Pmax);
				//std::cout << m_generators[idFuel].fuels[j].Pmin << " " << m_generators[idFuel].fuels[j].Pmax << std::endl;				
			}
		}
	}
	returnValue = fscanf(file, "A COST COEFFICIENTS\n");
	//std::cout << " A cost coeficients "<< std::endl;
	for(i = 0; i < m_numberOfGenerators; ++i) 
	{
		if(m_constraintMultipleFuels) 
		{
			for(j = 0; j < m_generators[i].nFuel; ++j) 
			{
				if(j != m_generators[i].nFuel - 1)
				{
					returnValue = fscanf(file, "%lf ", &m_generators[i].fuels[j].a);
					//std::cout << m_generators[i].fuels[j].a << std::endl;
				}
				else 
				{
					returnValue = fscanf(file, "%lf\n", &m_generators[i].fuels[j].a);
					//std::cout << m_generators[i].fuels[j].a << std::endl;
				}
			}
		} 
		else
		{
			returnValue = fscanf(file, "%lf\n", &m_generators[i].a);
			//std::cout << m_generators[i].a << std::endl;
		}
	}
	returnValue = fscanf(file, "B COST COEFFICIENTS\n");
	//std::cout << " A cost coeficients "<< std::endl;
	for(i = 0; i < m_numberOfGenerators; ++i) {
		if(m_constraintMultipleFuels) {
		  for(j = 0; j < m_generators[i].nFuel; ++j) {
			if(j != m_generators[i].nFuel - 1)
			{
				returnValue = fscanf(file, "%lf ", &m_generators[i].fuels[j].b);
				//std::cout << m_generators[i].fuels[j].b << std::endl;
			}
			else
			{
				returnValue = fscanf(file, "%lf\n", &m_generators[i].fuels[j].b);
				//std::cout << m_generators[i].fuels[j].b << std::endl;
			}
		  }
		} else
		{
			returnValue = fscanf(file, "%lf\n", &m_generators[i].b);
			//std::cout << m_generators[i].b << std::endl;
		}
	}
	returnValue = fscanf(file, "C COST COEFFICIENTS\n");
	//std::cout << " C cost coeficients "<< std::endl;
	  for(i = 0; i < m_numberOfGenerators; ++i) {
		if(m_constraintMultipleFuels) {
		  for(j = 0; j < m_generators[i].nFuel; ++j) {
			if(j != m_generators[i].nFuel - 1)
			{
				returnValue = fscanf(file, "%lf ", &m_generators[i].fuels[j].c);
			//std::cout << m_generators[i].fuels[j].c << std::endl;
			}
			else
			{
				returnValue = fscanf(file, "%lf\n", &m_generators[i].fuels[j].c);
			//std::cout << m_generators[i].fuels[j].c << std::endl;
			}
		  }
		} else 
		{
			returnValue = fscanf(file, "%lf\n", &m_generators[i].c);
			//std::cout << m_generators[i].c << std::endl;
		}
	  }
	  
	  if(m_constraintValvePointEffects) {
    returnValue = fscanf(file, "E COST COEFFICIENTS\n");
	//std::cout << " E cost coeficients "<< std::endl;
    for(i = 0; i < m_numberOfGenerators; ++i) {
      if(m_constraintMultipleFuels) {
        for(j = 0; j < m_generators[i].nFuel; ++j) {
          if(j != m_generators[i].nFuel - 1)
          {
			  returnValue = fscanf(file, "%lf ", &m_generators[i].fuels[j].e);
			//std::cout << m_generators[i].fuels[j].e << std::endl;
		  }
          else 
          {
			  returnValue = fscanf(file, "%lf\n", &m_generators[i].fuels[j].e);
			//std::cout << m_generators[i].fuels[j].e << std::endl;
		  }
        }
      } else
      {
		  returnValue = fscanf(file, "%lf\n", &m_generators[i].e);
			//std::cout << m_generators[i].e << std::endl;
	  }
    }
    returnValue = fscanf(file, "F COST COEFFICIENTS\n");
	//std::cout << " F cost coeficients "<< std::endl;
    for(i = 0; i < m_numberOfGenerators; ++i) {
      if(m_constraintMultipleFuels) {
        for(j = 0; j < m_generators[i].nFuel; ++j) {
          if(j != m_generators[i].nFuel - 1)
          {
			  returnValue = fscanf(file, "%lf ", &m_generators[i].fuels[j].f);
			//std::cout << m_generators[i].fuels[j].f << std::endl;
		  }
          else
          {
			  returnValue = fscanf(file, "%lf\n", &m_generators[i].fuels[j].f);
			//std::cout << m_generators[i].fuels[j].f << std::endl;
		  }
        }
      } else 
      {
		  returnValue = fscanf(file, "%lf\n", &m_generators[i].f);
			//std::cout << m_generators[i].f << std::endl;
	  }
    }
  }
  
  if(m_constraintRampRateLimits) {
    returnValue = fscanf(file, "PREVIOUS OUTPUT POWERS\n");
    for(i = 0; i < m_numberOfGenerators; ++i) returnValue = fscanf(file, "%lf\n", &m_generators[i].P0);
    returnValue = fscanf(file, "UPRAMP LIMITS\n");
    for(i = 0; i < m_numberOfGenerators; ++i) {
      returnValue = fscanf(file, "%lf\n", &m_generators[i].UR);
      /* Update it to save some time in fitness evaluation. */
      m_generators[i].UR = fmin(m_generators[i].Pmax, m_generators[i].P0 + m_generators[i].UR);
    }
    returnValue = fscanf(file, "DOWNRAMP LIMITS\n");
    for(i = 0; i < m_numberOfGenerators; ++i) {
      returnValue = fscanf(file, "%lf\n", &m_generators[i].DR);
      /* Update it to save some time in fitness evaluation. */
      m_generators[i].DR = fmax(m_generators[i].Pmin, m_generators[i].P0 - m_generators[i].DR);
    }
  }
  
  if(m_constraintProhibitedZones) {
    returnValue = fscanf(file, "PROHIBITED ZONES\n");
    returnValue = fscanf(file, "Number of generators with prohibited zones: %i\n", &nGPZ);
    for(i = 0; i < nGPZ; ++i) {
      returnValue = fscanf(file, "%i ", &idGPZ);
      idGPZ--;
      returnValue = fscanf(file, "%i\n", &m_generators[idGPZ].nPZ);
      m_generators[idGPZ].pzMin = (double*)malloc(m_generators[idGPZ].nPZ * sizeof(double));
      m_generators[idGPZ].pzMax = (double*)malloc(m_generators[idGPZ].nPZ * sizeof(double));
      for(j = 0; j < m_generators[idGPZ].nPZ; ++j)
        returnValue = fscanf(file, "%lf %lf\n", &m_generators[idGPZ].pzMin[j], &m_generators[idGPZ].pzMax[j]);
    }
  }
  
  if(m_constraintPowerLosses) {
    returnValue = fscanf(file, "B LOSS COEFFICIENTS\n");
    for(i = 0; i < m_numberOfGenerators; ++i) {
      m_generators[i].B = (double*)malloc(m_numberOfGenerators * sizeof(double));
      for(j = 0; j < m_numberOfGenerators; ++j) returnValue = fscanf(file, "%lf\n", &m_generators[i].B[j]);
    }
    returnValue = fscanf(file, "B0 LOSS COEFFICIENTS\n");
    for(i = 0; i < m_numberOfGenerators; ++i) returnValue = fscanf(file, "%lf\n", &m_generators[i].B0);
    returnValue = fscanf(file, "B00 LOSS COEFFICIENT\n");
    returnValue = fscanf(file, "%lf\n", &m_lossCoefficient);
  }

}

void Eld::printParameters()
{
	Utils utils;
	std::cout << "|---------------------------|" << std::endl;
	std::cout << "| Economic Load Dispatch    |" << std::endl;
	std::cout << "|---------------------------|" << std::endl;
	std::cout << "| Number of generators: " << m_numberOfGenerators << std::endl;
	std::cout << "| Total demand        : " << m_totalDemandSystem << std::endl;
	std::cout << "|---------------------------|" << std::endl;
	std::cout << "| Constraints               |" << std::endl;
	std::cout << "|---------------------------|" << std::endl;
	std::cout << "| Power losses        : " << utils.toString(m_constraintPowerLosses) << std::endl;
	std::cout << "| Prohibited zones    : " << utils.toString(m_constraintProhibitedZones) << std::endl;
	std::cout << "| Ramp rate limits    : " << utils.toString(m_constraintRampRateLimits) << std::endl;
	std::cout << "| Valve point effects : " << utils.toString(m_constraintValvePointEffects) << std::endl;
	std::cout << "| Multiple fuels      : " << utils.toString(m_constraintMultipleFuels) << std::endl;
	std::cout << "|---------------------------|" << std::endl;
	std::cout << "| Penalty Parameters        |" << std::endl;
	std::cout << "|---------------------------|" << std::endl;
	std::cout << "| Penalty balance         : " << m_penaltyBalance << std::endl;
	std::cout << "| Penalty capacities      : " << m_penaltyCapacities << std::endl;
	std::cout << "| Penalty ramp rate limits: " << m_penaltyRampRateLimits << std::endl;
	std::cout << "| Penalty prohibited zones: " << m_penaltyProhibitedZones << std::endl;
	std::cout << "|---------------------------|" << std::endl;
	std::cout << "| MINIMUM PRODUCTION LIMITS |" << std::endl;
	std::cout << "|---------------------------|" << std::endl;
	for(unsigned short int i = 0; i < m_numberOfGenerators; ++i)
	{
			std::cout << "| " << m_generators[i].Pmin << std::endl;
	}
	std::cout << "|---------------------------|" << std::endl;
	std::cout << "| MAXIMUM PRODUCTION LIMITS |" << std::endl;
	std::cout << "|---------------------------|" << std::endl;
	for(unsigned short int i = 0; i < m_numberOfGenerators; ++i)
	{
			std::cout << "| " << m_generators[i].Pmax << std::endl;
	}
}

double Eld::evaluate(double x[], int size)
{
	int i, j;
    double total_generation_cost, total_power_output, limits, objective, power_loss, prohibited_zones, ramp_limits;

	/* Total cost and total power output calculation */
	total_generation_cost = 0;
	total_power_output = 0;
	for(i = 0; i < m_numberOfGenerators; ++i) {
    if(m_constraintMultipleFuels) {
      for(j = 0; j < m_generators[i].nFuel; ++j) {
        if(m_generators[i].Pmin == m_generators[i].fuels[j].Pmin && x[i] <= m_generators[i].fuels[j].Pmax) break;
        else if(m_generators[i].Pmax == m_generators[i].fuels[j].Pmax && x[i] > m_generators[i].fuels[j].Pmin) break;
        else if(x[i] > m_generators[i].fuels[j].Pmin && x[i] <= m_generators[i].fuels[j].Pmax) break;
      }

      total_generation_cost += m_generators[i].fuels[j].a * x[i] * x[i] +
                               m_generators[i].fuels[j].b * x[i] +
                               m_generators[i].fuels[j].c;
      if(m_constraintValvePointEffects) total_generation_cost += fabs(m_generators[i].fuels[j].e *
                     sin(m_generators[i].fuels[j].f * (m_generators[i].fuels[j].Pmin - x[i])));
    } else {
      total_generation_cost += m_generators[i].a * x[i] * x[i] + m_generators[i].b * x[i] + m_generators[i].c;
      if(m_constraintValvePointEffects) total_generation_cost += fabs(m_generators[i].e * sin(m_generators[i].f * (m_generators[i].Pmin - x[i])));
    }
    total_power_output += x[i];
  }
  
  /* Generating unit capacities (max and min) */
  limits = 0;
  for(i = 0; i < m_numberOfGenerators; ++i) {
    limits += fabs(x[i] - m_generators[i].Pmin) - (x[i] - m_generators[i].Pmin);
    limits += fabs(m_generators[i].Pmax - x[i]) - (m_generators[i].Pmax - x[i]);
  }
  
  /* Power losses calculation */
  power_loss = 0;
  if(m_constraintPowerLosses) {
    power_loss = m_lossCoefficient;
    for(i = 0; i < m_numberOfGenerators; ++i) {
      for(j = 0; j < m_numberOfGenerators; ++j) power_loss += x[i] * m_generators[i].B[j] * x[j];
      power_loss += x[i] * m_generators[i].B0;
    }
    power_loss = power_loss / 100; /* Must be divided by 100, because the loss coefficients are in 100-Mva base. */
  }
  
  /* Prohibited zones */
  if(m_constraintProhibitedZones) {
    prohibited_zones = 0;
    for(i = 0; i < m_numberOfGenerators; ++i)
      for(j = 0; j < m_generators[i].nPZ; ++j)
        if(m_generators[i].pzMin[j] < x[i] && x[i] < m_generators[i].pzMax[j])
          prohibited_zones += fmin(x[i] - m_generators[i].pzMin[j], m_generators[i].pzMax[j] - x[i]);
  }
  
   /* Generating unit ramp rate limits (min and max) */
  if(m_constraintRampRateLimits) {
    ramp_limits = 0;
    for(i = 0; i < m_numberOfGenerators; ++i) {
      ramp_limits += fabs(m_generators[i].UR - x[i]) - (m_generators[i].UR - x[i]);
      ramp_limits += fabs(x[i] - m_generators[i].DR) - (x[i] - m_generators[i].DR);
    }
  }
  
  objective = total_generation_cost;
  objective += m_penaltyBalance * fabs(total_power_output - m_totalDemandSystem - power_loss);
  objective += m_penaltyCapacities * limits;
  if(m_constraintProhibitedZones) objective += m_penaltyProhibitedZones * prohibited_zones;
  if(m_constraintRampRateLimits) objective += m_penaltyRampRateLimits * ramp_limits;
  
	return objective;
}

std::string Eld::getName()
{
	return "EconomicLoadDispatch"+m_prefix;// : File "+m_file;
}


double Eld::verifyBounds(int index, double d)
{
	if(d > getUpperBound(index))
	{
		return getUpperBound(index);
	}
	else if(d < getLowerBound(index))
	{
		return getLowerBound(index);
	}
	return d;
}

double Eld::getLowerBound(int index)
{
	return m_generators[index].Pmin;
}

double Eld::getUpperBound(int index)
{
	return m_generators[index].Pmax;
}

#endif
