#ifndef METAHEURISTIC_FACTORY_H
#define METAHEURISTIC_FACTORY_H

#include <stdexcept>
#include <string>

#include "Metaheuristic.hpp"
#include "continuous/ArtificialBeeColonyOptimization.hpp"
#include "continuous/DifferentialEvolution.hpp"
#include "continuous/SymbioticOrganismSearch.hpp"
#include "continuous/ImprovedSymbioticOrganismSearch.hpp"
#include "continuous/ParticleSwarmOptimization.hpp"
#include "continuous/BatAlgorithm.hpp"

class MetaheuristicFactory{
public:
	MetaheuristicFactory(Configuration config){m_config = config;}
	Metaheuristic * get(std::string name, int index);
private:
	Configuration m_config;
};

Metaheuristic * MetaheuristicFactory::get(std::string name, int index)
{
	if(name == "ABC")
	{
		return new ArtificialBeeColonyOptimization(m_config);
	}
	else if(name == "DE")
	{
		return new DifferentialEvolution(m_config);
	}
	else if(name == "ABC_DE")
	{
		if((index % 2) == 0)
		{
			return get("ABC",index);
		}
		else
		{
			return get("DE",index);
		}
	}
	else if(name == "SOS")
	{
		return new SymbioticOrganismSearch(m_config);
	}
	else if(name == "PSO")
	{
		return new ParticleSwarmOptimization(m_config);
	}
	else if(name == "I_SOS")
	{
		return new ImprovedSymbioticOrganismSearch(m_config);
	}
	else if(name == "BAT")
	{
		return new BatAlgorithm(m_config);
	}
	throw std::invalid_argument( "received invalid Metaheuristic name" );
}

#endif
