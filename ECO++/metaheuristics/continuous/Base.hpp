#ifndef DIFFERENTIAL_EVOLUTION_H
#define DIFFERENTIAL_EVOLUTION_H

#include <string>

#include "../Metaheuristic.hpp"

class DifferentialEvolution : public Metaheuristic{
public:
	DifferentialEvolution(Configuration config)
	{
		m_config = config;
	};
	~DifferentialEvolution()
	{
	};
	virtual void execute(Specie* specie);
	virtual string getName();
private:
	Configuration m_config;
	Aleatory m_aleatory;
	Organism m_organism;
};

void DifferentialEvolution::execute(Specie* specie)
{
	//===============================
	// Init Evolutionary Period
	//===============================
	specie->initEvolutionaryPeriod();

	while(specie->hasEvolutionaryPeriod())
	{
		
	}
	
	//===============================
	// Finish Evolutionary Period
	//===============================
	specie->finishEvolutionaryPeriod();
}

string DifferentialEvolution::getName()
{
	return "Differential Evolution";
}

#endif
