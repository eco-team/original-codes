#ifndef PARTICLE_SWARM_OPTIMIZATION_H
#define PARTICLE_SWARM_OPTIMIZATION_H

#include <string>

#include "../Metaheuristic.hpp"

class ParticleSwarmOptimization : public Metaheuristic{
public:
	ParticleSwarmOptimization(Configuration config)
	{
		unsigned short int d = 0;
		m_config = config;
		m_pbest = new Organism[m_config.getSpecieSize()];
		m_velocity = new Organism[m_config.getSpecieSize()];
		
		for(unsigned short int a =0; a < m_config.getSpecieSize(); a++)
		{
				m_velocity[a] = Organism(m_config.getDimensions());
				for(d = 0; d < m_config.getDimensions(); d++)
				{
					m_velocity[a][d] = m_aleatory.nextDouble(m_config.getObjectiveFunction()->getLowerBound(d),m_config.getObjectiveFunction()->getUpperBound(d));
				}
		}
		
		m_flag = false;
		//===========================
		// parameters
		//===========================
		m_w = 0.721;
		m_c1 = 1.193;
		m_c2 = 1.193;
	};
	~ParticleSwarmOptimization()
	{
		delete [] m_velocity;
		delete [] m_pbest;
	};
	virtual void execute(Specie* specie);
	virtual string getName();
private:
	Organism * m_velocity;
	Organism * m_pbest;
	Configuration m_config;
	Aleatory m_aleatory;
	Organism m_organism;
	bool m_flag;
	double m_c1;
	double m_c2;
	double m_w;
};

void ParticleSwarmOptimization::execute(Specie* specie)
{
	unsigned short int i = 0;
	unsigned short int d = 0;
	unsigned short int index_gbest;
	double temp= 0;
	//===============================
	// Init Evolutionary Period
	//===============================
	specie->initEvolutionaryPeriod();
	
	//===============================
	// memorize pbest
	//===============================
	if(!m_flag)
	{
		//=============================
		// to first execution
		//=============================
		for(i =0; i < m_config.getSpecieSize(); i++)
		{
			m_pbest[i] = specie->operator[](i); 
		}
		m_flag = true;
	}
	else
	{
		for(i =0; i < m_config.getSpecieSize(); i++)
		{
			if(specie->operator[](i).getObjectiveFunction() < m_pbest[i].getObjectiveFunction())
			{
				m_pbest[i] = specie->operator[](i);
			}
		}
	}
	
	//=======================
	// gbest position
	//=======================
	index_gbest = specie->getCurrentBestIndex();

	while(specie->hasEvolutionaryPeriod())
	{		
		for(i =0; i < m_config.getSpecieSize(); i++)
		{
			for(d = 0; d < m_config.getDimensions(); d++)
			{
				m_velocity[i][d] = (m_w * m_velocity[i][d]) + 
				(m_c1 * m_aleatory.nextDouble() * (m_pbest[i][d] - specie->operator[](i)[d])) +
				(m_c2 * m_aleatory.nextDouble() * (m_pbest[index_gbest][d] - specie->operator[](i)[d]));
				specie->operator[](i)[d] = specie->operator[](i)[d] + m_velocity[i][d];
				//==============================
				// set limits
				//==============================	
				specie->operator[](i)[d] = m_config.getObjectiveFunction()->verifyBounds(d,specie->operator[](i)[d]);	
				
			}
			
			specie->operator[](i).evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());			
			specie->incrementEvaluations();
			
			if(specie->operator[](i).getObjectiveFunction() < m_pbest[i].getObjectiveFunction())
			{
				m_pbest[i] = specie->operator[](i);
				if(m_pbest[i].getObjectiveFunction() < m_pbest[index_gbest].getObjectiveFunction())
				{
					index_gbest = i;
				}
			}
		}
		//===============================
		// Find current best
		// to memorize global best
		//===============================
		specie->findCurrentBest();
	}
	
	//===============================
	// Finish Evolutionary Period
	//===============================
	specie->finishEvolutionaryPeriod();
}

string ParticleSwarmOptimization::getName()
{
	return "Particle Swarm Optimization";
}

#endif
