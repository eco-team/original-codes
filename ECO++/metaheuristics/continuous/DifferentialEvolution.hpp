#ifndef DIFFERENTIAL_EVOLUTION_H
#define DIFFERENTIAL_EVOLUTION_H

#include <string>

#include "../Metaheuristic.hpp"

class DifferentialEvolution : public Metaheuristic{
public:
	DifferentialEvolution(Configuration config)
	{
		m_config = config;
		m_dimens = m_config.getDimensions();
		m_size = m_config.getSpecieSize();
		m_fitFunction = m_config.getFitnessFunction();
		m_objFunction = m_config.getObjectiveFunction();
		m_crossover = 0.9;
		m_factor = 0.8;
		m_organism = Organism(config.getDimensions());
	};
	~DifferentialEvolution()
	{
	};
	virtual void execute(Specie* specie);
	virtual string getName();
private:
	Configuration m_config;
	Aleatory m_aleatory;
	Organism m_organism;
	double m_crossover;
	double m_factor;
	int m_dimens;
	int m_size;
	ObjectiveFunction * m_objFunction;
	FitnessFunction * m_fitFunction;
};

void DifferentialEvolution::execute(Specie* specie)
{
	unsigned short int index = 0;
	unsigned short int n = 0;
	unsigned short int L = 0;
	unsigned short int r1 = 0;
	unsigned short int r2 = 0;
	unsigned short int r3 = 0;
	bool hasEvaluations = true;
	double tmp = 0.0;
	//===============================
	// Init Evolutionary Period
	//===============================
	specie->initEvolutionaryPeriod();

	do
	{
		for(index = m_size; index--; )
		{
			r1 = m_aleatory.nextInt(m_size);
			r2 = m_aleatory.nextInt(m_size);
			r3 = m_aleatory.nextInt(m_size);
			
			while(r1 == index)
			{
				r1 = m_aleatory.nextInt(m_size);
			}
			
			while(r2 == index || r1 == r2)
			{
				r2 = m_aleatory.nextInt(m_size);
			}
			
			while(r3 == index || r1 == r3 || r2 == r3 )
			{
				r3 = m_aleatory.nextInt(m_size);
			}
			
			//===============================
			// rand/1/bin
			//===============================
			n = m_aleatory.nextInt(m_dimens);
			
			for(L=m_dimens; L--;)
			{
				
				//===============================
				//change at least one parameter 
				//===============================
				if (m_aleatory.nextDouble() < m_crossover || L == 0) 
				{ 
					tmp = specie->operator[](r1)[n] + m_factor * (specie->operator[](r2)[n] - specie->operator[](r3)[n]);
					//==============================
					// set limits
					//==============================	
					m_organism[n] = m_objFunction->verifyBounds(n,tmp);						
				}
				else
				{
						m_organism[n] = specie->operator[](index)[n];
				}
				n = ( ++n - ( n / m_dimens ) * m_dimens );
				//x%y == (x-(x/y)*y)
			}
			
			//==============================
			// Evaluate
			//==============================
			m_organism.evaluate(m_objFunction,m_fitFunction);
			hasEvaluations = specie->incrementEvaluations();
			
			if(m_organism.getObjectiveFunction() < specie->operator[](index).getObjectiveFunction())
			{
				specie->operator[](index) = m_organism;
			}
			
			if(!hasEvaluations)
			{
				break;
			}
			
		}
	}while(hasEvaluations);
	
	//===============================
	// Finish Evolutionary Period
	//===============================
	specie->finishEvolutionaryPeriod();
}

string DifferentialEvolution::getName()
{
	return "Differential Evolution";
}

#endif
