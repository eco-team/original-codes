#ifndef IMPROVED_SYMBIOTIC_ORGANISM_SEARCH_H
#define IMPROVED_SYMBIOTIC_ORGANISM_SEARCH_H

#include <string>

#include "../Metaheuristic.hpp"

class ImprovedSymbioticOrganismSearch : public Metaheuristic{
public:
	ImprovedSymbioticOrganismSearch(Configuration config)
	{
		m_config = config;
		m_organismA = Organism(m_config.getDimensions());
	};
	~ImprovedSymbioticOrganismSearch()
	{
	};
	virtual void execute(Specie* specie);
	virtual string getName();
private:
	Configuration m_config;
	Aleatory m_aleatory;
	Organism m_organismA;
};

void ImprovedSymbioticOrganismSearch::execute(Specie* specie)
{
	unsigned short int index = 0;	
	unsigned short int other = 0;
	unsigned short int j = 0;
	unsigned short int currentBestIndex = 0;
	bool hasEvaluations = true;
	double tmp = 0.0;
	
	//===============================
	// Init Evolutionary Period
	//===============================
	specie->initEvolutionaryPeriod();

	while(hasEvaluations)
	{
		
		for(index = specie->size(); index--; )
		{
			currentBestIndex = specie->getCurrentBestIndex();
			
			//===============================
			// Commensalism phase
			//===============================
			do
			{
				other = m_aleatory.nextInt(specie->size());
			}while(other == index);
			
			j = m_aleatory.nextInt(m_config.getDimensions());
			
			//for(j = m_config.getDimensions(); j-- ; )
			//{
					tmp = specie->operator[](index)[j] + m_aleatory.nextDouble(-1,1) * (specie->operator[](currentBestIndex)[j] - specie->operator[](other)[j]);
					m_organismA[j] = m_config.getObjectiveFunction()->verifyBounds(j,tmp);	
			//}
			
			m_organismA.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());
			hasEvaluations = specie->incrementEvaluations();
			
			if(m_organismA.getObjectiveFunction() < specie->operator[](index).getObjectiveFunction())
			{
				specie->operator[](index) = m_organismA;
			}
			
			if(!hasEvaluations)
			{
				break;
			}
			
			
			//===============================
			// Parasitism phase
			//===============================
			do
			{
				other = m_aleatory.nextInt(specie->size());
			}while(other == index);
			
			m_organismA = specie->operator[](index);
			
			j = m_aleatory.nextInt(m_config.getDimensions());
			m_organismA[j] = m_aleatory.nextDouble(m_config.getObjectiveFunction()->getLowerBound(j),m_config.getObjectiveFunction()->getUpperBound(j));
			
			m_organismA.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());
			hasEvaluations = specie->incrementEvaluations();
			
			if(m_organismA.getObjectiveFunction() < specie->operator[](other).getObjectiveFunction())
			{
				specie->operator[](other) = m_organismA;
			}
			
			//===============================
			// Find current best
			//===============================
			specie->findCurrentBest();
			
			if(!hasEvaluations)
			{
				break;
			}
		}
	}
	
	//===============================
	// Finish Evolutionary Period
	//===============================
	specie->finishEvolutionaryPeriod();
}

string ImprovedSymbioticOrganismSearch::getName()
{
	return "Improved Symbiotic Organism Search";
}

#endif
