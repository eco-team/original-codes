#ifndef ALEATORY_H
#define ALEATORY_H

#include<stdlib.h>

class Aleatory{
public:
	double nextDouble();
	double nextDouble(double low, double high);
	int nextInt();
	int nextInt(int n);
	int nextInt(int low, int high);
};

double Aleatory::nextDouble()
{
	return nextDouble(0,1);
}

double Aleatory::nextDouble(double low, double high)
{
	return low + ((double)rand() / RAND_MAX) * ( high - low );
}

int Aleatory::nextInt()
{
	return nextInt(0,1);
}

int Aleatory::nextInt(int n)
{
	return (int) ((double)0.0 + ((n - 0.0)*rand()/(RAND_MAX+1.0)));
}

int Aleatory::nextInt(int low, int high)
{
	return nextInt(high-low +1) + low;
}

#endif
