#ifndef ECOSYSTEM_H
#define ECOSYSTEM_H

#include <thread>
#include <vector>
#include <algorithm>

#include "Specie.hpp"
#include "Habitats.hpp"
#include "Configuration.hpp"
#include "objectiveFunction/ObjectiveFunction.hpp"
#include "fitnessFunction/FitnessFunction.hpp"
#include "reports/Status.hpp"

struct pareto{   
	double fo_a;
	double fo_b;
	unsigned short int specie_a;
	unsigned short int specie_b;
	unsigned short int index_a;
	unsigned short int index_b;
	unsigned short int rank;
};

class Ecosystem{
public:
	Ecosystem(){
		m_species = 0;
	};
	Ecosystem(Configuration config);
	~Ecosystem();
	void init();
	Specie * get(unsigned short int index);
	Organism getGlobalBest();
	Status getStatus();
	void phaseHabitat(int runIndex, int sucessionIndex);
	void printHabitatSize(int runIndex);
	void symbioticInteractions();
	double getAvgEvaluations();
private:
	unsigned short int m_size;
	Configuration m_config;
	struct pareto * Pareto;
	Specie * m_species;
	//void* m_raw_memory;	
	std::string m_prefixFileName;
	std::vector<int> m_numHabitats;
	void canonicalIntraHabitatsInteractions();
	void performIntraHabitatsInteractions();
	void performInterHabitatsInteractions();
	void performMutualism();
	void performCompetition();
	void performAltruism();
	void performSlavery();
	void performSymbioticInteractions(unsigned short int specieA, unsigned short int specieB);
	Aleatory m_aleatory;
	Organism m_organism;
	Organism m_organism_b;
	int m_sucessions;
	class BackgroundTask
	{
	public:
		BackgroundTask(){
			m_specie = 0;
		};
		BackgroundTask(Specie * specie, ObjectiveFunction * obj, FitnessFunction * fit)
		{
			m_specie = specie; m_obj = obj; m_fit = fit;
		};
		void run()
		{
			if(m_specie != 0)
				m_specie->init(m_obj,m_fit);
		};
	private:
		Specie * m_specie;
		ObjectiveFunction * m_obj;
		FitnessFunction * m_fit;
	};
};

Ecosystem::Ecosystem(Configuration config)
{
	m_config = config;
}

Ecosystem::~Ecosystem()
{
	if(m_species != 0)
	{
		delete [] m_species;		
		delete [] Pareto;
		m_species = 0;
	}
}

void Ecosystem::init()
{
	
	m_numHabitats.reserve(m_config.getEcologicalSucessions());
	m_size = m_config.getSpecieNumber();
	
	Pareto = new struct pareto[m_config.getSpecieSize()];
	m_sucessions = 0;
	
	m_species = new Specie[m_size];
	std::thread threads[m_size];
	BackgroundTask tasks[m_size];
	for(unsigned short int a = m_size; a--; )
	{
		m_species[a] = Specie( m_config.getSpecieSize(),m_config.getDimensions(), m_config.getEvolutionaryPeriod(), m_config.getMaxEvaluationsFunctions(), m_config.getPhenotypic(), m_config.getGenotypic() );
		
		tasks[a] = BackgroundTask(&m_species[a],m_config.getObjectiveFunction(),m_config.getFitnessFunction());
		threads[a] = std::thread(&BackgroundTask::run,&tasks[a]);
	}
	for(unsigned short int a = m_size; a--; )
	{
		threads[a].join();
	}
	
}

Specie * Ecosystem::get(unsigned short int index)
{
	return &m_species[index];
}

Organism Ecosystem::getGlobalBest()
{
	Organism o;
	o = m_species[0].getGlobalBest();
	for(unsigned short int a = 1; a < m_size; a++)
	{
		if(m_species[a].getGlobalBest().getObjectiveFunction() < o.getObjectiveFunction())
			o = m_species[a].getGlobalBest();
	}
	return o;
}

Status Ecosystem::getStatus()
{
	Status status;
	double objective = 0;
	double genotypic = 0;
	double phenotypic = 0;
	for(unsigned short int a = 0; a < m_size; a++)
	{
		objective += m_species[a].getGlobalBest().getObjectiveFunction();
		genotypic += m_species[a].getGenotypicDiversity();
		phenotypic += m_species[a].getPhenotypicDiversity();
	}
	objective /= m_size;
	genotypic /= m_size;
	phenotypic /= m_size;
	status.setObjectiveFunction(objective);
	status.setGenotypicDiversity(genotypic);
	status.setPhenotypicDiversity(phenotypic);
	return status;
}

//===============================================================
// Funções para criar os habitats no arquivo Habitats.hpp
//===============================================================
void Ecosystem::phaseHabitat(int runIndex, int sucessionIndex)
{
	if(!m_config.isHabitats())
	{
		return;
	}	
	
	
	Utils utils;
	m_prefixFileName = "-run-"+utils.toString(runIndex)+"-ecoStep-"+utils.toString(sucessionIndex);

	
	buildDistanceMatrix(m_size, m_config.getDimensions(), m_species);
	
	
	buildDendogram(m_size, m_config.getDimensions());

	if(m_config.isSingleLinkClustering())
	{
		printDendogram(m_config.getDirOutput()+"/habitats/"+m_config.getObjectiveFunction()->getName()+m_prefixFileName+".singleLinkageClustering",m_size);
	}

	
	buildHabitats(m_size);	

	m_numHabitats.push_back(g_habitatsSize);

	if(m_config.isSingleLinkClustering())
	{
		printPairwiseInteractions(m_config.getDirOutput()+"/habitats/"+m_config.getObjectiveFunction()->getName()+m_prefixFileName+".singleLinkageClustering",m_size);
	}
	
	
	performIntraHabitatsInteractions();
	
	performInterHabitatsInteractions();

	for(unsigned short int a = 0; a < m_size; a++)
	{
		m_species[a].findCurrentBest();
	}

}

void Ecosystem::performIntraHabitatsInteractions()
{
	if(m_config.isSymbioticRelationship())
	{
		symbioticInteractions();
	}
	else
	{
		canonicalIntraHabitatsInteractions();
	}

}

void Ecosystem::symbioticInteractions()
{
	unsigned short int a = 0;
	for(a = 0; a < m_size; a++)
	{
		if(sp_int[a] > 0)
		{
				performSymbioticInteractions(a, sp_adj[a][m_aleatory.nextInt(sp_int[a])]);
		}
	}
	m_sucessions++;	
}

void Ecosystem::performSymbioticInteractions(unsigned short int specieA, unsigned short int specieB)
{
	unsigned short int k = 0;
	unsigned short int j = 0;
	int typeSymbiotic = -1;
	double m_phi = 0.0;
	for(k = 0; k <  m_config.getSpecieSize(); k++) // para cada individuo da população i
	{
		Pareto[k].specie_a = specieA;
		Pareto[k].specie_b = specieB;
		Pareto[k].index_a  = k;
		Pareto[k].index_b  = m_aleatory.nextInt(m_config.getSpecieSize()); // escolhe aleatoriamente um individuo da outra espécie
		Pareto[k].fo_a     = m_species[specieA][k].getObjectiveFunction();
		Pareto[k].fo_b     = m_species[specieB][Pareto[k].index_b].getObjectiveFunction();
		Pareto[k].rank     = 1;
	}
	//-------------------------------------
	// Verificar rank dos pares 
	//------------------------------------- 
	if(m_config.getTypeSymbioticRelationship() == "ALL")
	{
			typeSymbiotic = m_aleatory.nextInt(4);
	}
	else if(m_config.getTypeSymbioticRelationship() == "MUTUALISM")
	{
		typeSymbiotic = 0;
	}
	else if(m_config.getTypeSymbioticRelationship() == "SLAVERY")
	{
		typeSymbiotic = 1;		
	}
	else if(m_config.getTypeSymbioticRelationship() == "COMPETITION")
	{
		typeSymbiotic = 2;		
	}
	else if(m_config.getTypeSymbioticRelationship() == "ALTRUISM")
	{
		typeSymbiotic = 3;		
	}
	else if(m_config.getTypeSymbioticRelationship() == "ADAPTED")
	{
		//Not working...
		/*bool a = m_species[specieA].getCurrentBest().getObjectiveFunction() > m_species[specieB].getCurrentBest().getObjectiveFunction();
		bool b = m_species[specieA].getPhenotypicDiversity() > m_species[specieB].getPhenotypicDiversity();
		if(a && b) // mutualismo
		{
			typeSymbiotic = 0;		
		}
		else if(a && !b) // esclavagismo
		{
			typeSymbiotic = 1;		
		}
		else if(!a && !b) // competição
		{
			typeSymbiotic = 2;		
		}
		else if(!a && b) // altruismo
		{
			typeSymbiotic = 3;		
		}*/
	}
	switch(typeSymbiotic)
	{
		case 0:
			performMutualism();
			break;
		case 1:
			performSlavery();
			break;
		case 2:
			performCompetition();
			break;
		case 3:
			performAltruism();
			break;
	}
	
	for(k = 0; k < m_config.getSpecieSize(); k ++)
	{
		//======================================
		// Population A
		//======================================
		if(Pareto[k].rank == 1)
		{
			m_organism = m_species[Pareto[k].specie_a][Pareto[k].index_a];
			m_organism_b = m_species[Pareto[k].specie_b][Pareto[k].index_b];
			for(j = 0; j < m_config.getDimensions(); j++)
			{
				if(m_aleatory.nextDouble() > 0.5)
				{
					m_organism[j] = m_species[Pareto[k].specie_b][Pareto[k].index_b][j];
					m_organism_b[j] = m_species[Pareto[k].specie_a][Pareto[k].index_a][j];
				}
			}
			
			//===
			// Calcular a distancia
			//===
			double p1_c1 = 0.0;
			double p2_c2 = 0.0;
			double p1_c2 = 0.0;
			double p2_c1 = 0.0;
			double prob_child_win = 0.0;
			double aux_1 = 0.0;
			double aux_2 = 0.0;
			int a = 0;
			int b = 0;
			
			for(a = 0; a < m_organism.size(); a++)
			{
				p1_c1 += fabs(m_organism[a] - m_species[Pareto[k].specie_a][Pareto[k].index_a][a]) * fabs(m_organism[a] - m_species[Pareto[k].specie_a][Pareto[k].index_a][a]);
			}
			for(a = 0; a < m_organism.size(); a++)
			{
				p2_c2 += fabs(m_organism_b[a] - m_species[Pareto[k].specie_b][Pareto[k].index_b][a]) * fabs(m_organism_b[a] - m_species[Pareto[k].specie_b][Pareto[k].index_b][a]);
			}
			for(a = 0; a < m_organism.size(); a++)
			{
				p1_c2 += fabs(m_organism[a] - m_species[Pareto[k].specie_b][Pareto[k].index_b][a]) * fabs(m_organism[a] - m_species[Pareto[k].specie_b][Pareto[k].index_b][a]);
			}			
			for(a = 0; a < m_organism.size(); a++)
			{
				p2_c1 += fabs(m_organism_b[a] - m_species[Pareto[k].specie_a][Pareto[k].index_a][a]) * fabs(m_organism_b[a] - m_species[Pareto[k].specie_a][Pareto[k].index_a][a]);
			}
			
			p1_c1 = sqrt(p1_c1);
			p2_c2 = sqrt(p2_c2);
			p1_c2 = sqrt(p1_c2);
			p2_c1 = sqrt(p2_c1);
			
			//===============
			// Inverte os filhos para competir
			//===============
			if((p1_c2 + p2_c1) > (p1_c1 + p2_c2))
			{
					Organism temp;
					temp = m_organism;
					m_organism = m_organism_b;
					m_organism_b = temp;
			}
			
			 
			if(m_species[Pareto[k].specie_a].getEvaluations() < m_config.getMaxEvaluationsFunctions())
			{
				m_organism.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());
				m_species[Pareto[k].specie_a].incrementEvaluations();
				
				//===
				// Calcula o valor de phi
				//===
				m_phi =  (m_species[Pareto[k].specie_a].getGenotypicDiversity() / m_species[Pareto[k].specie_a].getFirstGenotypicDiversity());
				
				//std::cout << "m_phi => " << m_phi << std::endl;
				
				//===
				// Calcula a probabilidade do filho substituir o pai
				//===
				aux_1 = pow(m_phi,1.0 / 1.0 + pow(exp(1.0), -(m_species[Pareto[k].specie_a][Pareto[k].index_a].getObjectiveFunction() - m_organism.getObjectiveFunction()))); 
				aux_2 = pow(m_phi,1.0 / 1.0 + pow(exp(1.0), -(m_organism.getObjectiveFunction() - m_species[Pareto[k].specie_a][Pareto[k].index_a].getObjectiveFunction()))); 
				
				//std::cout << "aux_1 => " << aux_1 << std::endl;
				//std::cout << "aux_2 => " << aux_1 << std::endl;
				
				prob_child_win = aux_1 * m_organism.getObjectiveFunction();
				
				
				//std::cout << "prob_child_win => " << prob_child_win << std::endl;
				
				prob_child_win /= prob_child_win + m_species[Pareto[k].specie_a][Pareto[k].index_a].getObjectiveFunction() * aux_2;
				
				//std::cout << prob_child_win << " ";
				
				//getchar();
				
				//if(m_organism.getObjectiveFunction() < Pareto[k].fo_a)
				if(prob_child_win > m_aleatory.nextDouble())
				{
					m_species[Pareto[k].specie_a][Pareto[k].index_a] = m_organism;
				}
			}
			//if(m_species[Pareto[k].specie_b].getEvaluations() < m_config.getMaxEvaluationsFunctions())
			if(prob_child_win > m_aleatory.nextDouble())
			{
				m_organism_b.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());
				m_species[Pareto[k].specie_b].incrementEvaluations();
				
				//===
				// Calcula o valor de phi
				//===
				m_phi =  (m_species[Pareto[k].specie_b].getGenotypicDiversity() / m_species[Pareto[k].specie_b].getFirstGenotypicDiversity());
				
				//===
				// Calcula a probabilidade do filho substituir o pai
				//===
				aux_1 = pow(m_phi,1.0 / 1.0 + pow(exp(1.0), -(m_species[Pareto[k].specie_b][Pareto[k].index_b].getObjectiveFunction() - m_organism.getObjectiveFunction()))); 
				aux_2 = pow(m_phi,1.0 / 1.0 + pow(exp(1.0), -(m_organism.getObjectiveFunction() - m_species[Pareto[k].specie_b][Pareto[k].index_b].getObjectiveFunction()))); 
				
				prob_child_win = aux_1 * m_organism.getObjectiveFunction();
				prob_child_win /= prob_child_win + m_species[Pareto[k].specie_b][Pareto[k].index_b].getObjectiveFunction() * aux_2;
				
				//std::cout << prob_child_win << " ";
				//if(m_organism_b.getObjectiveFunction() < Pareto[k].fo_b)
				if(prob_child_win > m_aleatory.nextDouble())
				{
					m_species[Pareto[k].specie_b][Pareto[k].index_b] = m_organism_b;					
				}
			}
		}
	}
}

void Ecosystem::performMutualism()
{
	unsigned short int k = 0;
	unsigned short int j = 0;
	//===========================================================================================
	// Dominância de Pareto
	//===========================================================================================
	for(k = 0; k < m_config.getSpecieSize(); k ++)
	{
		for(j = 0; j < m_config.getSpecieSize(); j++)
		{
			//===========================================================================
			// Critério de Minimização e Minimização
			//===========================================================================
			if(Pareto[k].fo_a > Pareto[j].fo_a && Pareto[k].fo_b > Pareto[j].fo_b)
			{
				Pareto[k].rank++;
			}
		} 
	}
}

void Ecosystem::performCompetition()
{
	unsigned short int k = 0;
	unsigned short int j = 0;
	//===========================================================================================
	// Dominância de Pareto
	//===========================================================================================
	for(k = 0; k < m_config.getSpecieSize(); k ++)
	{
		for(j = 0; j < m_config.getSpecieSize(); j++)
		{
			//===========================================================================
			// Critério de Maximização e Maximização
			//===========================================================================
			if(Pareto[k].fo_a < Pareto[j].fo_a && Pareto[k].fo_b < Pareto[j].fo_b)
			{
				Pareto[k].rank++;
			}
		} 
	}
}

void Ecosystem::performAltruism()
{
	unsigned short int k = 0;
	unsigned short int j = 0;
	//===========================================================================================
	// Dominância de Pareto
	//===========================================================================================
	for(k = 0; k < m_config.getSpecieSize(); k ++)
	{
		for(j = 0; j < m_config.getSpecieSize(); j++)
		{
			//===========================================================================
			// Critério de Minimização e Maximização
			//===========================================================================
			if(Pareto[k].fo_a > Pareto[j].fo_a && Pareto[k].fo_b < Pareto[j].fo_b)
			{
				Pareto[k].rank++;
			}
		} 
	}
}

void Ecosystem::performSlavery()
{
	unsigned short int k = 0;
	unsigned short int j = 0;
	//===========================================================================================
	// Dominância de Pareto
	//===========================================================================================
	for(k = 0; k < m_config.getSpecieSize(); k ++)
	{
		for(j = 0; j < m_config.getSpecieSize(); j++)
		{
			//===========================================================================
			// Critério de Maximização e Minimização
			//===========================================================================
			if(Pareto[k].fo_a < Pareto[j].fo_a && Pareto[k].fo_b > Pareto[j].fo_b)
			{
				Pareto[k].rank++;
			}
		} 
	}
}

void Ecosystem::canonicalIntraHabitatsInteractions()
{
	unsigned short int indexA;
	unsigned short int indexB;
	unsigned short int temp = 0;
	unsigned short int a = 0;
	unsigned short int b = 0;
	unsigned short int d = 0;
	unsigned short int specieB = 0;
	Organism organism;
	for(a = 0; a < m_size; a++)
	{
		if(sp_int[a] > 0)
		{
		
			indexA = m_aleatory.nextInt(m_config.getSpecieSize());
			for(b = 1; b < m_config.getTournamentSize(); b++)
			{
				temp = m_aleatory.nextInt(m_config.getSpecieSize());
				if(m_species[a][temp].getObjectiveFunction() < m_species[a][indexA].getObjectiveFunction())
				{
					indexA = temp;
				}
			}

			specieB = sp_adj[a][m_aleatory.nextInt(sp_int[a])]; 

			indexB = m_aleatory.nextInt(m_config.getSpecieSize());
			for(b = 1; b < m_config.getTournamentSize(); b++)
			{
				temp = m_aleatory.nextInt(m_config.getSpecieSize());
				if(m_species[specieB][temp].getObjectiveFunction() < m_species[specieB][indexB].getObjectiveFunction())
				{
					indexB = temp;
				}
			}			

			if(m_species[specieB].getEvaluations() <  m_config.getMaxEvaluationsFunctions())
			{
				organism = m_species[a][indexA];

				for(b = 0; b < m_config.getDimensions(); b++)
				{
					if(m_aleatory.nextDouble() > 0.5)
					{
						organism[b] = m_species[specieB][indexB][b];
					}
				}

				organism.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());

				do
				{
					indexB = m_aleatory.nextInt(m_config.getSpecieSize());
				}while(indexB == m_species[specieB].getCurrentBestIndex());

				m_species[specieB][indexB] = organism;
				m_species[specieB].incrementEvaluations();
			}
		}
	}
}

void Ecosystem::performInterHabitatsInteractions()
{
	unsigned short int specieA;
	unsigned short int specieB;
	unsigned short int indexB;
	if(g_habitatsSize == 1)
	{
		return;
	}
	for(unsigned short int i = 0; i < g_habitatsSize; i++)
	{
		specieA = H[i].h_sp[ m_aleatory.nextInt(H[i].h_sp_count) ];

		do
		{
			specieB = m_aleatory.nextInt(g_habitatsSize); 
		}while(specieB == i);
		specieB = H[specieB].h_sp[ m_aleatory.nextInt(H[specieB].h_sp_count) ];

		do
		{
			indexB = m_aleatory.nextInt(m_config.getSpecieSize());
		}while(indexB == m_species[specieB].getCurrentBestIndex());
		
		m_species[specieB][indexB] = m_species[specieA].getCurrentBest();
	}	
}

void Ecosystem::printHabitatSize(int runIndex)
{
	Utils utils;
	std::string fileName = m_config.getDirOutput()+"/habitats/"+m_config.getObjectiveFunction()->getName()+"-run-"+utils.toString(runIndex)+".numbersOfHabitats";
	std::ofstream output(fileName.c_str());
	output << "#=====================================#" << std::endl;
	output << "# ECO_STEP # NUMBER OF HABITATS       #" << std::endl;
	output << "#=====================================#" << std::endl;
	for(unsigned short int i = 0; i < m_numHabitats.size(); i ++)
	{
		output << i << " " << m_numHabitats[i] << std::endl;
	}
}

double Ecosystem::getAvgEvaluations()
{
	double eval = 0;
	for(unsigned short i = 0; i < m_size; i++)
	{
		eval += (double)m_species[i].getEvaluations();
	}
	return eval / (double)m_size;
}

#endif
