#ifndef STATUS_H
#define STATUS_H


class Status{
public:
	void setObjectiveFunction(double objectiveFunction);
	double getObjectiveFunction();
	void setGenotypicDiversity(double genotypicDiversity);
	double getGenotypicDiversity();
	void setPhenotypicDiversity(double phenotypicDiversity);
	double getPhenotypicDiversity();
private:
	double m_objectiveFunction;
	double m_genotypicDiversity;
	double m_phenotypicDiversity;
};

void Status::setObjectiveFunction(double objectiveFunction)
{
	m_objectiveFunction = objectiveFunction;
}

double Status::getObjectiveFunction()
{
	return m_objectiveFunction;
}

void Status::setGenotypicDiversity(double genotypicDiversity)
{
	m_genotypicDiversity = genotypicDiversity;
}

double Status::getGenotypicDiversity()
{
	return m_genotypicDiversity;
}

void Status::setPhenotypicDiversity(double phenotypicDiversity)
{
	m_phenotypicDiversity = phenotypicDiversity;
}

double Status::getPhenotypicDiversity()
{
	return m_phenotypicDiversity;
}


#endif

