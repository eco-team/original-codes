#ifndef RESULT_H
#define RESULT_H


#include <vector>

#include"Status.hpp"

class Result{
public:
	Result(int size){m_list.reserve(size);};
	void setBestGlobalObjectiveFunction(double bestGlobalobjectiveFunction);
	double getBestGlobalObjectiveFunction();
	void add(Status status);
	Status getStatus(int index);
	void setEvaluations(double evaluations);
	double getEvaluations();
	void setTime(double time);
	double getTime();
private:
	double m_bestGlobalobjectiveFunction;
	double m_evaluations;
	double m_time;
	std::vector<Status> m_list;
};

void Result::setEvaluations(double evaluations)
{
	m_evaluations = evaluations;
}

double Result::getEvaluations()
{
	return m_evaluations;
}

void Result::setTime(double time)
{
	m_time = time;
}

double Result::getTime()
{
	return m_time;
}

void Result::setBestGlobalObjectiveFunction(double bestGlobalobjectiveFunction)
{
	m_bestGlobalobjectiveFunction = bestGlobalobjectiveFunction;
}

double Result::getBestGlobalObjectiveFunction()
{
	return m_bestGlobalobjectiveFunction;
}

void Result::add(Status status)
{
	m_list.push_back(status);
}

Status Result::getStatus(int index)
{
	return m_list[index];
}

#endif
