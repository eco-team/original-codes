#ifndef FITNESS_FACTORY_H
#define FITNESS_FACTORY_H

#include <stdexcept>
#include <string>

#include "FitnessFunction.hpp"
#include "DefaultFitnessFunction.hpp"

class FitnessFactory{
public:
	FitnessFunction * get(std::string name);
};

FitnessFunction * FitnessFactory::get(std::string name)
{
	if(name == "DEFAULT")
	{
		return new DefaultFitnessFunction();
	}
	throw std::invalid_argument( "received invalid Fitness Function name" );
}

#endif
