#ifndef FITNESS_FUNCTION_H
#define FITNESS_FUNCTION_H

#include<string>

class FitnessFunction{
public:
	virtual double evaluate(double objectiveFunction) = 0;
	virtual std::string getName() = 0;
};

#endif
