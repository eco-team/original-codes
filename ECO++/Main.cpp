#include <iostream>

#include "EcologicalInspiratedAlgorithm.hpp"

int main(int argc, char** argv)
{
	
	if(argc == 1)
	{
		std::cout << " No have input file " << std::endl;
	}

	for (unsigned short int i = 1; i < argc; i++) {		

		EcologicalInspiratedAlgorithm eco(argv[i]);
		eco.execute();
	}	

}
