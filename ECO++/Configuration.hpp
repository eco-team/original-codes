#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <iostream>
#include <string>

#include "diversity/genotypic/GenotypicDiversityMeasure.hpp"
#include "diversity/phenotypic/PhenotypicDiversityMeasure.hpp"
#include "Utils.hpp"


class Configuration{
public:
	unsigned short int getDimensions()
	{
		return m_dimens;
	};
	void setDimensions(unsigned short int d)
	{
		m_dimens = d;
	};
	unsigned short int getSpecieSize()
	{
		return m_specieSize;
	};
	void setSpecieSize(unsigned short int d)
	{
		m_specieSize = d;
	};
	unsigned short int getSpecieNumber()
	{
		return m_specieNumber;
	};
	void setSpecieNumber(unsigned short int d)
	{
		m_specieNumber = d;
	};
	unsigned int getEvolutionaryPeriod()
	{
		return m_evolutionaryPeriod;
	};
	void setEvolutionaryPeriod(unsigned int d)
	{
		m_evolutionaryPeriod = d;
	};
	unsigned int getEcologicalSucessions()
	{
		return m_ecologicalSucessions;
	};
	void setEcologicalSucessions(unsigned int d)
	{
		m_ecologicalSucessions = d;
	};
	unsigned int getMaxEvaluationsFunctions()	
	{
		return m_ecologicalSucessions * m_evolutionaryPeriod;
	};
	FitnessFunction * getFitnessFunction()
	{
		return m_fitFunction;
	};
	void setFitnessFunction(FitnessFunction * f)
	{
		m_fitFunction = f;
	};
	ObjectiveFunction * getObjectiveFunction()
	{
		return m_objFunction;
	};
	void setObjectiveFunction(ObjectiveFunction * o)
	{
		m_objFunction = o;
	};
	void setRuns(unsigned short int d)
	{
		m_runs = d;
	};
	unsigned short int getRuns()
	{
		return m_runs;
	};
	void setStrategy(std::string s)
	{
		m_strategy = s;
	};
	std::string getStrategy()
	{
		return m_strategy;
	};
	void setDirOutput(std::string s)
	{
		m_dirOutput = s;
	};
	std::string getDirOutput()
	{
		return m_dirOutput;
	};
	bool isConvergence()
	{
		return m_convergence;
	};
	void setConvergence(bool b)
	{
		m_convergence = b;
	};
	bool isResult()
	{
		return m_result;
	};
	void setResult(bool b)
	{
		m_result = b;
	};	
	void setGenotypic(GenotypicDiversityMeasure * s)
	{
		m_genotypic = s;
	};
	GenotypicDiversityMeasure * getGenotypic()
	{
		return m_genotypic;
	};
	void setPhenotypic(PhenotypicDiversityMeasure * s)
	{
		m_phenotypic = s;
	};
	PhenotypicDiversityMeasure * getPhenotypic()
	{
		return m_phenotypic;
	};
	bool isHabitats()
	{
		return m_habitats;
	};
	void setHabitats(bool b)
	{
		m_habitats = b;
	};
	bool isSingleLinkClustering()
	{
		return m_singleLinkClustering;
	};
	void setSingleLinkClustering(bool b)
	{
		m_singleLinkClustering = b;
	};
	bool isNumberHabitats()
	{
		return	m_numberHabitats;
	};
	void setNumberHabitats(bool b)
	{
		m_numberHabitats = b;
	};
	bool isSymbioticRelationship()
	{
		return m_symbioticRelationship;
	};
	void setSymbioticRelationship(bool b)
	{
		m_symbioticRelationship = b;
	};
	unsigned short int getTournamentSize()
	{
		return m_tournamentSize;
	};
	void setTournamentSize(unsigned short int b)
	{
		m_tournamentSize = b;
	};
	bool isGlobalBest()
	{
		return m_globalBest;
	};
	void setGlobalBest(bool b)
	{
		m_globalBest = b;
	};
	std::string getTypeSymbioticRelationship()
	{
			return m_typeSymbioticRelationship;
	}
	void setTypeSymbioticRelationship(std::string type)
	{
			m_typeSymbioticRelationship = type;
	}
	void print();
private:
	unsigned short int m_dimens;
	unsigned short int m_specieSize;
	unsigned short int m_specieNumber;
	unsigned int m_evolutionaryPeriod;
	unsigned int m_ecologicalSucessions;
	unsigned short int m_runs;
	FitnessFunction * m_fitFunction;
	ObjectiveFunction * m_objFunction;
	std::string m_strategy;
	std::string m_dirOutput;
	GenotypicDiversityMeasure * m_genotypic;
	PhenotypicDiversityMeasure * m_phenotypic;
	bool m_convergence;
	bool m_result;
	bool m_habitats;
	bool m_singleLinkClustering;
	bool m_numberHabitats;
	bool m_symbioticRelationship;
	bool m_globalBest;
	unsigned short int m_tournamentSize;
	std::string m_typeSymbioticRelationship;
};

void Configuration::print()
{
	Utils utils;
	std::cout << "|======================================|" << std::endl;
	std::cout << "|            CONFIGURATION             |" << std::endl;
	std::cout << "|======================================|" << std::endl;
	std::cout << "| RUNS                     : " << m_runs << std::endl;
	std::cout << "|--------------------------------------|" << std::endl;
	std::cout << "| ECOLOGICAL SUCESSIONS    : " << m_ecologicalSucessions << std::endl;
	std::cout << "| EVOLUTIONARY PERIOD      : " << m_evolutionaryPeriod << std::endl;
	std::cout << "| SPECIE NUMBER            : " << m_specieNumber << std::endl;
	std::cout << "| SPECIE SIZE              : " << m_specieSize << std::endl;
	std::cout << "|--------------------------------------|" << std::endl;
	std::cout << "| DIMENSIONS               : " << m_dimens << std::endl;
	std::cout << "| MAX FUNCTION EVALUATIONS : " << getMaxEvaluationsFunctions() << std::endl;
	std::cout << "| OBJECTIVE FUNCTION       : " << m_objFunction->getName() << std::endl;
	std::cout << "| FITNESS FUNCTION         : " << m_fitFunction->getName() << std::endl;
	std::cout << "|--------------------------------------|" << std::endl;
	std::cout << "| GENOTYPIC DIVERSITY      : " << m_genotypic->getName() << std::endl;
	std::cout << "| PHENOTYPIC DIVERSITY     : " << m_phenotypic->getName() << std::endl;
	std::cout << "|--------------------------------------|" << std::endl;
	std::cout << "| HABITATS                 : " << utils.toString(m_habitats) << std::endl;
	std::cout << "| TOURNAMENT SIZE          : " << m_tournamentSize << std::endl;
	std::cout << "| SYMBIOTIC RELATIONSHIP   : " << utils.toString(m_symbioticRelationship) << std::endl;
	std::cout << "| TYPE SYMBIOTIC           : " << m_typeSymbioticRelationship << std::endl;
	std::cout << "|--------------------------------------|" << std::endl;
	std::cout << "| STRATEGY                 : " << m_strategy << std::endl;
	std::cout << "|--------------------------------------|" << std::endl;
	std::cout << "| DIRECTORY OUTPUT         : " << m_dirOutput << std::endl;
	std::cout << "| REPORT CONVERGENCE       : " << utils.toString(m_convergence) << std::endl;
	std::cout << "| REPORT RESULTS           : " << utils.toString(m_result) << std::endl;
	std::cout << "| REPORT DENDOGRAM         : " << utils.toString(m_singleLinkClustering) << std::endl;
	std::cout << "| REPORT NUMBER HABITATS   : " << utils.toString(m_numberHabitats) << std::endl;
	std::cout << "| REPORT GLOBAL BEST       : " << utils.toString(m_globalBest) << std::endl;
	std::cout << "|--------------------------------------|" << std::endl;
}

#endif
